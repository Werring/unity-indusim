#pragma strict
class Players extends NetworkBehaviour {
    private var _playerList:Hashtable = new Hashtable();
    var me:Player;

	// Start
    function Start () {
        super.Start();
    }

	// Handle connection.
    function OnConnect(){
        me = Player.getInstance();

        registerRPC("announceToAllPlayers",announceToAllPlayers);
        registerRPC("announceReplyPlayer",announceReplyPlayer);
        sendRPC("announceToAllPlayers("+networking.userID+","+me.name+")",RPCMode.Others);
        registerPlayer(networking.userID,me.name);
    }

	// Reconnect
    function OnReconnect(){
        OnConnect();
    }

	// Announce to all players
    function announceToAllPlayers(s:String[],player:int){
        var playerName:String = s[1];
        var playerID:int = int.Parse(s[0]);
        registerPlayer(playerID,playerName);
        sendRPC("announceReplyPlayer("+networking.userID+","+me.name+")",player);
    }

	// Reply to player
    function announceReplyPlayer(s:String[],player){
        var playerName:String = s[1];
        var playerID:int = int.Parse(s[0]);
        registerPlayer(playerID,playerName);
    }

	// Add player to list
    function registerPlayer(playerID:int, playerName:String){
        _playerList[playerID] = playerName;
    }

	// Get player list
    function get playerList():Hashtable{
        return _playerList;
    }
}
