#pragma strict
class NetworkBehaviour extends MonoBehaviour{
    protected var networking:Networking;
    protected var className:String = "";

	// Initialize
    function Start(){
        className = (typeof this).ToString();
        networking = GameObject.Find("NetworkScripts").GetComponent(Networking);
        StartCoroutine(listenForConnect());
    }


    function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo){}

    function OnConnect(){}

    function OnReconnect(){}

	// Register to networking.
    function registerStateSync(){
        networking.register(this);
    }

	// Register remote procedure;
    protected function registerRPC(funcName:String,func:Function){
        networking.registerRPC(className, funcName, func);
    }

	// Send remote procedure
    protected function sendRPC(funcName:String,mode:RPCMode){
        networking.callRPC(className,funcName,mode);
    }

	// Send remote procedure
    protected function sendRPC(funcName:String,player:int){
        networking.callRPC(className,funcName,player);
    }

	// Listen for connections.
    private function listenForConnect(){
        yield WaitForSeconds(0.2);
        networking.addListenForConnect(this);
    }
}
