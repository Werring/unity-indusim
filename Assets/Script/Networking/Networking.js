#pragma strict
class Networking extends MonoBehaviour {

    var showGUI:boolean = false;

    var connectToIP:String = "GET_LAN_IP";
    var connectPort:int = 25001;

    private var _userID:int = -1;

    private var _playerConnections:Hashtable;
    private var _uid = 0;

    private var networkClasses:Array;
    private var rpc_functions:Hashtable;
    private var connectListenClasses:Array;

    private var date:int = 0;

    private var _connected:boolean = false;

    public function get userID(){
        return _userID;
    }

    /**
     * Initialize variables
     */
	function Awake() {
        networkClasses =  new Array();
        rpc_functions = new Hashtable();
        connectListenClasses = new Array();
        _playerConnections = new Hashtable();
	}
	/**
     * GUI for connection
     * @Depricated
     */
    function OnGUI() {
        if(showGUI){
            if(Application.loadedLevel == 0){
                if(connectToIP == "GET_LAN_IP"){
                    connectToIP = Network.player.ipAddress;
                }

                if (Network.peerType == NetworkPeerType.Disconnected){
                    GUILayout.Label("Connection status: Disconnected");

                    connectToIP = GUILayout.TextField(connectToIP, GUILayout.MinWidth(100));
                    connectPort = parseInt(GUILayout.TextField(connectPort.ToString()));

                    GUILayout.BeginVertical();
                    if (GUILayout.Button ("Connect as client")){
                        Network.Connect(connectToIP, connectPort);
                    }

                    if (GUILayout.Button ("Start Server")){
                        Network.InitializeServer(10, connectPort, false);
                    }
                    GUILayout.EndVertical();
                }else{
                    if (Network.peerType == NetworkPeerType.Connecting){
                        GUILayout.Label("Connection status: Connecting");
                    }else if (Network.peerType == NetworkPeerType.Client){
                        GUILayout.Label("Connection status: Client!");
                        GUILayout.Label("Ping to server: "+Network.GetAveragePing(  Network.connections[0] ) );
                    }else if (Network.peerType == NetworkPeerType.Server){
                        GUILayout.Label("Connection status: Server!");
                        GUILayout.Label("Connections: "+Network.connections.length);
                        if(Network.connections.length>=1){
                            GUILayout.Label("Ping to first player: "+Network.GetAveragePing(  Network.connections[0] ) );
                        }
                    }

                    if (GUILayout.Button ("Disconnect")){
                        Network.Disconnect(200);
                    }
                }
            }
        }
    }

	function InitializeServer(maxPlayers:int, port:int) {
		Network.InitializeServer(maxPlayers, port, false);
	}

	function Disconnect() {
		Network.Disconnect();
	}

	function Connect(ip:String, port:int) {
		Network.Connect(ip, port);
	}

	function OnPlayerDisconnected(player:NetworkPlayer) {
		Debug.Log("Clean up after player " + player);
		_playerConnections.Remove(player.ToString());

		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}


    /**
     * Translate NetworkPlayer to _userID
     */
	private function networkPlayerToUID(networkPlayer:NetworkPlayer):int{
	    return int.Parse((_playerConnections[networkPlayer.ToString()] as Array)[0].ToString());
	}

	/**
	 * Translate _userID to NetworkPlayer
	 */
	private function UIDToNetworkPlayer(uid:int){
	    var nwplayer:NetworkPlayer;
        for(var key:Object in _playerConnections.Keys){
            var player:Array = _playerConnections[key] as Array;
            if(player[0] == uid){
                nwplayer = player[1];
                return nwplayer;
            }
        }
        throw(new System.Exception("Player not found"));
	}

    /**
     * Server has been started or restarted
     */
    function OnServerInitialized(){
        Debug.Log("Server initialized and ready");
        if(_userID == -1){
            playerID(++_uid);
        }else{
            serverReconnected();
        }
    }

    /**
     * Client connected
     */
	function OnConnectedToServer() {
        Debug.Log("This CLIENT has connected to a server");
        if(_userID == -1){
            networkView.RPC("getPlayerID",RPCMode.Server);
        }else{
            serverReconnected();
        }
    }

    /**
     * Activate all OnConnect functions in NetworkBehaviours
     */
    function serverConnected(){
        for(var i:int; i<connectListenClasses.length;i++){
            (connectListenClasses[i] as NetworkBehaviour).OnConnect();
        }
        _connected = true;
    }

    /**
     * activates all onReconnect functions in NetworkBehaviours
     * and resends acceptPlayerID to server to update _playerConnections
     */
    function serverReconnected(){
        networkView.RPC("acceptPlayerID",RPCMode.Server,_userID);
        for(var i:int; i<connectListenClasses.length;i++){
            (connectListenClasses[i] as NetworkBehaviour).OnReconnect();
        }
    }

    /**
     * Serialized networkview for data that has to stay up to date
     */
    function OnSerializeNetworkView(stream:BitStream, info:NetworkMessageInfo){
        for(var i:int = 0;i<networkClasses.length;i++){
            var networkClass:NetworkBehaviour = networkClasses[i] as NetworkBehaviour;
            networkClass.OnSerializeNetworkView(stream,info);
        }
    }

    /**
     * Add all NetwoekBehaviours to this array
     */
    function addListenForConnect(networkClass:NetworkBehaviour){
        if(_connected){
            networkClass.OnConnect();
        }
        connectListenClasses.Push(networkClass);
    }

    /**
     * return current NetworkPlayer
     */
    function get networkPlayer(){
        return Network.player;
    }

    /**
     * get networkView
     */
    function getNetworkView(){
        return networkView;
    }

    /**
     * Register networkClasses & functions with Networking class
     */
    function register(networkClass:MonoBehaviour){
        networkClasses.Push(networkClass);
    }

    /**
     * register RPCFunctions
     */
    function registerRPC(networkClass:String, funcName:String, func:Function){
        try {
            rpc_functions.Add(networkClass + "_" + funcName,func);
        } catch(err:System.Exception){}
    }


    /**
     * execute RPC functions with RPCMode
     */
    function callRPC(networkClass:String,funcName:String,indusimRPCMode:RPCMode){
        if(Network.isClient){
            switch(indusimRPCMode){
                case RPCMode.All:
                case RPCMode.AllBuffered:
                    networkView.RPC("networkRPCAll",RPCMode.Server,networkClass+ "_" +funcName);
                break;
                case RPCMode.Others:
                case RPCMode.OthersBuffered:
                    networkView.RPC("networkRPCOthers",RPCMode.Server,networkClass+ "_" +funcName);
                break;
                case RPCMode.Server:
                    networkView.RPC("networkRPC",RPCMode.Server,networkClass+ "_" +funcName,_userID);
                break;
            }
        }
        if(Network.isServer){
               networkView.RPC("networkRPC",indusimRPCMode,networkClass+ "_" +funcName,_userID);
        }
    }

    /**
     * execute RPC functions with _userID
     */
    function callRPC(networkClass:String,funcName:String,indusimPlayer:int){
        if(Network.isClient){
            networkView.RPC("networkRPCPrivate",RPCMode.Server,indusimPlayer,networkClass+ "_" +funcName);
        }
        if(Network.isServer){
            var targetNetworkPlayer:NetworkPlayer;
            
            for(var key:Object in _playerConnections.Keys){
                var player:Array = _playerConnections[key] as Array;
                if(player[0] == indusimPlayer){
                    targetNetworkPlayer = player[1];
                    networkView.RPC("networkRPC",targetNetworkPlayer,networkClass+ "_" +funcName,_userID);
                }
            }
        }
    }

    /**
     * Recipient function handler
     */

     /**
      * Actual RPC handler on the clients
      * to translate functionstring to actual function
      * ========================
      * + Client
      */
    @RPC
    function networkRPC(class_funcName_params:String,sender:int,info:NetworkMessageInfo){
        var splitted:String[] = class_funcName_params.Split(["("],System.StringSplitOptions.None);
        var class_funcName:String;
        if(splitted.length > 1 && splitted[1].length > 1){
            class_funcName = splitted[0];
            var rest:String = splitted[1];
            rest = rest.Substring(0,rest.length-1);
            var params:String[] = rest.Split(","[0]);
            if(rpc_functions.ContainsKey(class_funcName)){
                (rpc_functions[class_funcName] as Function)(params,sender);
            }
        }else{
            class_funcName = splitted[0];
            if(rpc_functions.ContainsKey(class_funcName)){
                (rpc_functions[class_funcName] as Function)(sender);
            }
        }
    }

    /**
     * Server Redirection
     * - All users
     * ========================
     * + Server
     */
    @RPC
    function networkRPCAll(class_funcName_params:String,info:NetworkMessageInfo){
        networkView.RPC("networkRPC",RPCMode.All,class_funcName_params,networkPlayerToUID(info.sender));
    }

    /**
     * Server Redirection
     * - All users except sender
     * ========================
     * + Server
     */
    @RPC
    function networkRPCOthers(class_funcName_params:String,info:NetworkMessageInfo){
        var sender:int = networkPlayerToUID(info.sender);
        for(var np:NetworkPlayer in Network.connections){
            if(np != info.sender){
                networkView.RPC("networkRPC",np,class_funcName_params,sender);
            }
        }
        networkRPC(class_funcName_params,sender,info);
    }

    /**
     * Server Redirection
     * - Send to specific user
     * ========================
     * + Server
     */
    @RPC
    function networkRPCPrivate(target:int,class_funcName_params:String,info:NetworkMessageInfo){
        if(target == _userID){
            networkRPC(class_funcName_params,networkPlayerToUID(info.sender),info);
        }else{
            networkView.RPC("networkRPC",UIDToNetworkPlayer(target),class_funcName_params,networkPlayerToUID(info.sender));
        }
    }

    /**
     * set _userID
     * - client
     * ========================
     * + Client
     */
    @RPC
    function playerID( id:int){
        if(_userID == -1){
            _userID = id;
            Debug.Log("I just got uid: " + _userID);
            networkView.RPC("acceptPlayerID",RPCMode.Server,_userID);
            serverConnected();
        }
    }

    /**
     * link NetworkPlayer with _userID
     * ========================
     * + Server
     */
    @RPC
    function acceptPlayerID( id:int, info:NetworkMessageInfo){
        if(_playerConnections.ContainsValue(id)){
        	var playerConnection:Array;
            for(var key:Object in _playerConnections.Keys){
            	playerConnection = _playerConnections[key] as Array;
                if(playerConnection[0] == id){
                    _playerConnections.Remove(key);
                }
            }
        }
        _playerConnections[info.sender.ToString()] = new Array(id,info.sender);
        Debug.Log(info.sender.ToString() + " just got #" + id);
    }

    /**
     * Give _userID to client
     * ========================
     * + Server
     */
    @RPC
    function getPlayerID(info:NetworkMessageInfo){
        if(networkView.isMine){
            networkView.RPC("playerID",info.sender,++_uid);
        }
    }

    function isMine(){
        return networkView.isMine;
    }
}