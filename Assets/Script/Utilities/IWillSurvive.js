#pragma strict

class IWillSurvive extends MonoBehaviour {
	// Ensures that GameObject is not destroyed when changing scene
	function Awake() {
		DontDestroyOnLoad(this.gameObject);
	}
}
