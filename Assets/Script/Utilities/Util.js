import JSONObject;

import System;

public class Util {

	static var DEBUG:boolean = false;

	// Rounds an double to closest int.
	static function mathRound(val:double){
		return Mathf.Floor(val + 0.5);
	}
	static function mathRound(val:double, precision:int){
    	return Mathf.Floor(val * Mathf.Pow(10, precision) + 0.5) / Mathf.Pow(10, precision);
	}

	// Gives an timestamp of current time
	static function getTime(){
		var epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		return timestamp;
	}

	// Creates an JSON string of all resources, including market variables
	static function resourcesToJSONString(resources:Array){
    	var json:String = "[";
    	for(var i:int = 0; i < resources.length; i++){
    		var res:MarketResource = resources[i];
    		var name:String = res.resourceName();
    		var startPrice:int = res.startPrice();
    		var demand:int = res.demand();
    		var supply:double = res.supply();
    		var demandConstant:double = res.demandConstant();
    		var eqilibrium:int = res.eqilibrium();
    		var population:double = Population.size();
    		var resourceJson = "{\"startPrice\":\"" + startPrice + "\"";
    		resourceJson += ",\"name\":\"" + name + "\"";
    		resourceJson += ",\"demand\":\"" + demand + "\"";
    		resourceJson += ",\"supply\":\"" + supply + "\"";
    		resourceJson += ",\"eqil\":\" " + eqilibrium + "\"";
    		resourceJson += ",\"demandConstant\":\"" + demandConstant + "\"}";
    		resourceJson += ",";

    		json += resourceJson;
    	}
    	json += "{\"Population\":" + population + "}]";
    	return json;
    }

	// converts and applies an JSON of all market values
    static function JSONToResources(json:String){
    	var mainJSON:JSONObject = new JSONObject(json);
    	var resourceArray = new Array();
    	var population:double;
    	for(var i:int = 0; i < mainJSON.list.Count; i++){
    		var subJSON:JSONObject = mainJSON.list[i];
    		if(subJSON.list.Count > 1){
    			var marketRes:MarketResource = JSONObjectToMarketResource(subJSON);
    			resourceArray.Push(marketRes);
    		}else{
    			var JSONPopulationObject:JSONObject = subJSON.list[0];
    			var JSONPopulationNumber:JSONObject = JSONPopulationObject;
    			population = JSONPopulationNumber.n;
    		}
    	}
    	for(var h:int = 0; h < resourceArray.length; h++){
    		var res:MarketResource = resourceArray[h];
    		res.calculateDemand(population);
    	}
    	Population.setSize(population);
    	return resourceArray;
    }

	// Converts an JSONObject to an market resource
	private static function JSONObjectToMarketResource(json:JSONObject){
		var name:String = "";
		var demand:int = 0;
		var supply:double = 0;
		var eqil:int = 0;
		var startPrice:int = 0;
		var demandConstant:double = 0;

		for(var k:int = 0; k < json.list.Count; k++){
			var key:String = json.keys[k];
			var j:JSONObject = json.list[k];
			//Debug.Log(j.str);
			switch(key){
				case "name":
					name = j.str;
					break;
				case "demand":
					demand = int.Parse(j.str);
					break;
				case "supply":
					supply = parseFloat(j.str);
					break;
				case "eqil":
					eqil = int.Parse(j.str);
					break;
				case "startPrice":
					startPrice = int.Parse(j.str);
					break;
				case "demandConstant":
					demandConstant = parseFloat(j.str);
					break;
			}

		}

		var res:MarketResource = new MarketResource(name, startPrice, eqil, supply, demandConstant);
		return res;
	}

	// creates an MD5 Hash of the input string
	static function MD5(strToEncrypt: String){
        var encoding = System.Text.UTF8Encoding();
        var bytes = encoding.GetBytes(strToEncrypt);

        // encrypt bytes
        var md5 = System.Security.Cryptography.MD5CryptoServiceProvider();
        var hashBytes:byte[] = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        var hashString = "";

        for (var i = 0; i < hashBytes.Length; i++){
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, "0"[0]);
        }

        return hashString.PadLeft(32, "0"[0]);
    }

	// Adds thousandssperator(s)
	static function thousandsSeparator(x: Object) {
        return String.Format("{0:N}", int.Parse(x.ToString().Split('.'[0])[0])).Split('.'[0])[0].Replace(",",".");
	}

	// Finds an GameObject by tag
	static function FindGameObjectsFromTags(tags:Array){
		var objects:Array = new Array();
		for(var i:int = 0; i < tags.length; i++){
			objects = objects.Concat(new Array(GameObject.FindGameObjectsWithTag(tags[i].ToString())));
		}
		return objects.ToBuiltin(GameObject) as GameObject[];
	}

}

