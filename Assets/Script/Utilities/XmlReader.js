/*#pragma strict


import System.Xml;
import System.IO;
	
class XmlReader {
	
	//dit moet echt anders
	//ik wil een  klasse kunnen meegeven en daarvan wordt een array van objecten gemaakt
	function Read (XMLFile : String, variables : Array) {
		try{
			var asset:TextAsset = Resources.Load("Xml/" + XMLFile) as TextAsset;
			var returnArray = new Array();
			if(asset != null)
			{
				var reader:XmlTextReader = new XmlTextReader(new StringReader(asset.text));
				while(reader.Read())
				{
					if(reader.Name == variables[0] as String)
					{
						for(var i = 1; i < variables.length; i++)
						{
							//Debug.Log("buildingTemplateName= " + reader.GetAttribute("buildingTemplateName") + " x= " + reader.GetAttribute("vectorX") + " y= " + reader.GetAttribute("vectorY") + " resourcenode= " + reader.GetAttribute("resourceNodeDependant"));
							returnArray.push(reader.GetAttribute(variables[i] as String));
						}
					}
					else if(reader.Name == "cost")
					{
						returnArray.push(new Resource(reader.GetAttribute("resourceName"), int.Parse(reader.GetAttribute("amount")), 0.0, 0.0));
					}
				}
			}
			return returnArray;
		} catch(err){
			Debug.Log(err);
		}
		return null;
	}
}*/

#pragma strict


import System.Xml;
import System.IO;
	
class XmlReader {
	
	//dit moet echt anders
	//ik wil een  klasse kunnen meegeven en daarvan wordt een array van objecten gemaakt
	function Read (XMLFile : String) {
		try{
			var reader:XmlDocument = new XmlDocument();
			reader.Load("Assets/Resources/Xml/" + XMLFile + ".xml");
			var firstNode:XmlNode = reader.FirstChild;
			
			var returnArray = new Array();
			for(var i = 0; i<firstNode.ChildNodes.Count; i++)
			{
				var node:XmlNode = firstNode.ChildNodes[i];
				var template = new Array();
				for(var j = 0; j<node.ChildNodes.Count; j++)
					template.push(node.ChildNodes[j].InnerText);
				returnArray.push(template);
			}
			return returnArray;
		} catch(err){
			Debug.Log(err);
		}
		return null;
	}
}