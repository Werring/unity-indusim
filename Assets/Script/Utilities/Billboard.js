#pragma strict

class Billboard extends MonoBehaviour{
    var Cam : Camera;
    // Initialize
    function Start (){
    	Cam = GameObject.Find("EyeSocket/Eye").GetComponent(Camera);
    }

    // Set cam on scene load
    function OnLevelWasLoaded(level:int){
     	if(level == 1){
			Cam = GameObject.Find("EyeSocket/Eye").GetComponent(Camera);
		}	
	}

    // Update rotation.
    function Update (){
    	if(Cam != null){
	    	var Vector : Vector3;
	    	Vector = Cam.transform.position;
	    	Vector.y = -Vector.y;
	    	transform.LookAt(Vector);
    	}
    }
}