#pragma strict

import System;
import System.IO;

class Stats extends MonoBehaviour {

	function Awake(){}

	// Initialize
	function Start(){
		generateXML();
	}

	// Generate XML
	function generateXML(){
		var hashCalculator:HashCalc = new HashCalc();
		var id:int  = UnityEngine.Random.Range(0,10000);
		var statXML = 	'<?xml version="1.0" encoding="utf-8"?>'+"\n";
			statXML +=	'<StatsData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'+"\n";
			statXML +=	'<id>'+id.ToString()+'</id>'+"\n";
		var stat:StatsStorage;
		var entry:StatItem;
		var stats:ICollection = StatsStorage.statList();
		for each(var i:Object in stats){
			stat = StatsStorage.getStat(i.ToString());
			statXML += "<graph>"+"\n";
			statXML += "<type>" + stat.type + "</type>"+"\n";
			statXML += "<title>" + i.ToString() + "</title>"+"\n";
			if(stat.labels){
				statXML += "<sort>label</sort>"+"\n";
			}
			statXML += "<entries>"+"\n";
			for(var j:int = 0; j < stat.entries.length;j++){
				entry = stat.entries[j] as StatItem;
				statXML += "<entry>"+"\n";
				statXML += "<label>" + entry.label + "</label>"+"\n";
				hashCalculator.addVal(entry.label);
				if(entry.hasMultipleValues()){
					statXML += "<xValue>" + entry.xValue + "</xValue>"+"\n";
					statXML += "<yValue>" + entry.yValue + "</yValue>"+"\n";
					hashCalculator.addVal(entry.xValue + ":" + entry.yValue);
				} else {
					hashCalculator.addVal(entry.val);
					statXML += "<value>" + entry.val + "</value>"+"\n";
				}
				hashCalculator.addSalt();
				statXML += "</entry>"+"\n";
			}
			statXML += "</entries>"+"\n";
			statXML += "</graph>"+"\n";
		}
		hashCalculator.addVal(id);
		statXML +="<hash>" + hashCalculator.generateHash() + "</hash>" + "\n";
		statXML +="</StatsData>";

		//print(statXML);

		var dir:String =  System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + Path.DirectorySeparatorChar;

		var sr:System.IO.StreamWriter = File.CreateText(dir + Player.getInstance().statfilename);

		sr.WriteLine(statXML);

		sr.Close();
	}
}
