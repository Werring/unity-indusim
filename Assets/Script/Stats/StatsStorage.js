#pragma strict
import LitJson;
import System.Text;

class StatsStorage {
    private static var stats:Hashtable = new Hashtable();

    public enum StatTypes {PLOT,PIE,BAR};

    private var _name:String;
    private var _type:StatTypes;
    private var _entries:Array = new Array();
    private var _labels:boolean = false;

	// Get stat.
    public static function getStat(name:String):StatsStorage{
        if(stats[name] == null){
            throw(new System.Exception("Stat "+name+" does not exist."));
        }
        return stats[name] as StatsStorage;
    }

	// Get stat keys.
    public static function statList(){
    	return stats.Keys;
    }

	// Get name
    public function get name():String{
        return _name;
    }

	// Get type
    public function get type():String{
        return _type.ToString().ToLower();
    }

	// Get entries
    public function get entries():Array{
        return _entries;
    }

	// Create stat
    public function StatsStorage(name:String,type:StatTypes){
    	if(stats[name] != null){
    		throw(new System.Exception("Stat can't be created twice!"));
    	}
        stats.Add(name,this);
        _name = name;
        _type = type;
    }

	// Add data with 1 int value
    public function addData(label:String,value:int){
        var statitem:StatItem = new StatItem();
        statitem.label = label;
        statitem.value = value;
        _entries.Push(statitem);
    }

	// Add data with 1 double value
    public function addData(label:String,value:double){
        var statitem:StatItem = new StatItem();
        statitem.label = label;
        statitem.dvalue = value;
        _entries.Push(statitem);
    }

	// Add data with 2 values
    public function addData(label:String,xvalue:int,yvalue:int){
        var statitem:StatItem = new StatItem();
        statitem.label = label;
        statitem.xValue = xvalue;
        statitem.yValue = yvalue;
        _entries.Push(statitem);
    }

	// Add data with 1 value and a merge option
    public function addData(label:String,value:int,merge:boolean){
        if(merge){
            for(var i:int=0;i<_entries.length;i++){
                if((_entries[i] as StatItem).label == label){
                    (_entries[i] as StatItem).value+=value;
                    i=-1;
                    break;
                }
            }
            if(i!=-1){
                addData(label,value);
            }
        } else {
            addData(label,value);
        }
    }

	// Add data with 1 double value and a merge option
    public function addData(label:String,value:double,merge:boolean){
        if(merge){
            for(var i:int=0;i<_entries.length;i++){
                if((_entries[i] as StatItem).label == label){
                    (_entries[i] as StatItem).dvalue+=value;
                    i=-1;
                    break;
                }
            }
            if(i!=-1){
                addData(label,value);
            }
        } else {
            addData(label,value);
        }
    }

	// Add data with 2 values and a merge option
    public function addData(label:String,xvalue:int,yvalue:int,merge:boolean){
        if(merge){
            for(var i:int=0;i<_entries.length;i++){
                if((_entries[i] as StatItem).label == label){
                    (_entries[i] as StatItem).xValue+=xvalue;
                    (_entries[i] as StatItem).yValue+=yvalue;
                    i=-1;
                    break;
                }
            }
            if(i!=-1){
                addData(label,xvalue,yvalue);
            }
        } else {
            addData(label,xvalue,yvalue);
        }
    }

	// Enable labels
    public function enableLabels(){
       _labels = true;
    }

	// Labels enabled?
    public function get labels():boolean{
        return _labels;
    }

	// Convert to JSON
    public function toJSON():String {
        var sb:StringBuilder = new System.Text.StringBuilder();
        var writer:JsonWriter = new JsonWriter(sb);

        writer.WriteObjectStart();
        writer.WritePropertyName("name");
        writer.Write(_name);
        writer.WritePropertyName("type");
        writer.Write(_type.ToString());
        writer.WritePropertyName("entries");
        writer.WriteArrayStart();
        var entry:StatItem;
        for(var i:int =0; i< _entries.length; i++){
            entry = _entries[i] as StatItem;
            writer.WriteObjectStart();
            writer.WritePropertyName("label");
            writer.Write(entry.label);
            if(entry.hasMultipleValues()){
                writer.WritePropertyName("xValue");
                writer.Write(entry.xValue);
                writer.WritePropertyName("yValue");
                writer.Write(entry.yValue);
            } else {
                writer.WritePropertyName("value");
                writer.Write(entry.value);
            }
            writer.WriteObjectEnd();
        }
        writer.WriteArrayEnd();
        writer.WriteObjectEnd();
        return sb.ToString();
    }
}
