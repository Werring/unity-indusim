#pragma strict
class HashCalc {
    private var salt:String = "|InduSIM|";
    private var hashString:String = "";

	// Add item to has string	(Combine ingrediënts),
	public function addVal(value:Object){
	    hashString += value.ToString();
	}

	// Salt hash string			(Flavour it),
	public function addSalt(){
	    hashString += salt;
	}

	// Hash genereren			(Cook it).
	public function generateHash():String{
	    Debug.Log(hashString);
	    return Util.MD5(hashString);
	}
}
