#pragma strict
class StatItem {
    public var label:String;
    public var value:int;
    public var xValue:int;
    public var yValue:int;
    public var dvalue:double;

	// Determine if the item has multiple values.
    public function hasMultipleValues():boolean{
        return (xValue != null && yValue != null && (value == null || dvalue == null));
    }

    public function get val():int
    {
        if(dvalue){
            value = Mathf.Floor(dvalue);
        }

        return value;
    }


}
