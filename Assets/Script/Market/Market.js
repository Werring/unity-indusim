#pragma strict

public class Market extends NetworkBehaviour
{
	private var marketRegistry:RegistryMarket;
	private var resources:Array;
	private var sellingConstant:double = 0.91;
	private var timestamp:int;
	private var tickCount:int = 0;
	public var extChance:int = 25;
	public var extCottonChance:int = 3;
	public var extSize:double = 1.25;
	public var extCottonSize:double = 5.00;
	private static var _instance:Market = null;
	public var populationEqilibrium = 0.2;
	private var player:Player;

	// Initialize
    function Awake(){
        if(GameObject.Find("/_globalScripts")){
            var globalMarketScripts:GameObject = GameObject.Find("/_globalScripts/_market");
            if(globalMarketScripts == null){
                this.gameObject.transform.parent = GameObject.Find("/_globalScripts").transform;
                DontDestroyOnLoad(this.gameObject);
            } else {
                Destroy(this.gameObject);
            }
		}
    }

	// Declare resources
	function OnConnect(){
		marketRegistry = RegistryMarket.getInstance();
	    if(networking.isMine()){
            var resourceArray:Array = new Array(
            new MarketResource("Granen",		5,	100, 30, 1),
            new MarketResource("Brood",			10,	100, 30, 1),
            new MarketResource("Katoen",		10,	100, 60, 1),
            new MarketResource("Textiel",		13,	100, 25, 1),
            new MarketResource("Vlees",			15,	100, 25, 1),
            new MarketResource("Klei",			15,	100, 25, 1),
            new MarketResource("Suikerbiet",	18,	100, 20, 1),
            new MarketResource("Kleding",		20,	100, 20, 1),
            new MarketResource("Gereedschap",	20,	100, 20, 1),
            new MarketResource("Staal",			25,	100, 20, 1),
            new MarketResource("Bakstenen",		25,	100, 20, 1),
            new MarketResource("Bier",			28,	100, 20, 1),
            new MarketResource("Suiker",		30,	100, 20, 1)
            );
            marketRegistry.setResourceMap(resourceArray);
            resources = marketRegistry.resourceMap();
		} else {
		    sendRPC("getResourcesFromHost",RPCMode.Server);
		}
		timestamp = Util.getTime();
		player = Player.getInstance();
	}

 	// Start market
    function Start(){
        super.Start();

        registerRPC("getResourcesFromHost"	,getResourcesFromHost);
        registerRPC("RPCBuyResource"		,RPCBuyResource);
        registerRPC("RPCSellResource"		,RPCSellResource);
        registerRPC("youBoughtIt"			,buyActualResourceRPC);
        registerRPC("youSoldIt"				,sellActualResourceRPC);
        registerRPC("sendResourceList"		,getResourceList);

    }

	// Get resources from host.
    function getResourcesFromHost(sendingPlayer:int){
        if(resources != null) {
            var res:String = Util.resourcesToJSONString(resources);
            sendRPC("sendResourceList("+res+")",sendingPlayer);
        }else{
            StartCoroutine(waitBeforeSendResList(sendingPlayer));
        }
    }

 	// Wait a second before getting resources again.
    function waitBeforeSendResList(sendingPlayer:int){
        yield WaitForSeconds(1);
        getResourcesFromHost(sendingPlayer);
    }

	// Set resources from string.
    function getResourceList(str:String[],sendingPlayer:int){
        var jsonString:String = "";
        for(var i:int = 0; i<str.length;i++){
        	jsonString+=str[i] + ",";
        }

        var resourceArray:Array = Util.JSONToResources(jsonString);
        marketRegistry.setResourceMap(resourceArray);
        resources = marketRegistry.resourceMap();
    }

	// Buy resources. Remote procedure.
    function RPCBuyResource(param:String[],sendingPlayer:int){
        var r:int = int.Parse(param[0]);
        var amount:int = int.Parse(param[1]);
        var total:int = int.Parse(param[2]);

        var resource:MarketResource = resources[r] as MarketResource;
            resource.setSupply(resource.supply() - amount);
        sendRPC("youBoughtIt("+r+","+amount+","+total+")",sendingPlayer);
    }

	// Sell resources. Remote procedure.
    function RPCSellResource(param:String[],sendingPlayer:int){
        var r:int = int.Parse(param[0]);
        var amount:int = int.Parse(param[1]);
        var total:int = int.Parse(param[2]);

        var resource:MarketResource = resources[r] as MarketResource;
            resource.setSupply(resource.supply() + amount);
        sendRPC("youSoldIt("+r+","+amount+","+total+")",sendingPlayer);
    }

	// Get export price
    function getExportPrice(r:int){
    	return Util.mathRound(getImportPrice(r, true) * sellingConstant);
    }

	// Update timestamp
    function update(){
        var guiTime = Util.getTime() - timestamp;
		if(guiTime > 1){
			tick();
			timestamp = Util.getTime();
		}
    }

 	// Market tick, update population and market.
    function tick(){
        if(!networking.isMine()) sendRPC("getResourcesFromHost",RPCMode.Server);
    	tickCount++;
    	if(tickCount % 10 == 0){
    		updatePopulation();
    		updateMarket();
    	}
    }

 	// Update population
    function updatePopulation(){
    	var ratio:double;
    	var demand:double = 0;
    	var supply:double = 0;
    	var p:double = Population.size();

    	for(var i:int = 0; i+1 < resources.length; i++){
    		var r:MarketResource = resources[i] as MarketResource;
    		r.calculateDemand(p);
    		demand += r.demand();
    		supply += r.supply();
    	}
    	ratio = supply / (demand*0.5);
    	if(ratio > 1){
    		var population:double = p + (ratio*populationEqilibrium);
    		Population.setSize(population);
    	}
    }

 	// Update market
    function updateMarket(){
    	for(var j:int; j < resources.length; j++){
    		var r:MarketResource = resources[j] as MarketResource;
    		var d:double = r.demand();
    		r.setSupply(r.supply() - (d/150));
    		if((d/r.supply()) > 2){
    			if(r.resourceName() == "Katoen"){
    				externalDelivery(r, extCottonChance, extCottonSize);
    			}else{
    				externalDelivery(r, extChance, extSize);
    			}
    		}
    	}
    }

 	// External delivery
    function externalDelivery(r:MarketResource, eChance:double, eSize:double){
            var rand:int = Random.Range(0, eChance);
    		if(Random.Range(0, eChance) == 1){

    			var addSupply:double;
    			var d:double = r.demand();
    			var s:double = r.supply();
    			addSupply = ((d - s) * eSize);
    			r.setSupply(s + Random.Range(1, addSupply));
    		}
    }

 	// Get import price for product.
    function getImportPrice(r:int, sell:boolean){
    	var resource:MarketResource = resources[r] as MarketResource;
		var relativeDemand:double = 0;
		var relativeSupply:double = 0;
		var eqilibrium:double = resource.eqilibrium();
		var demand:double = Mathf.Floor(resource.demand());
		var supply:double = Mathf.Floor(resource.supply());
		if(sell){
		    supply ++;
		}
		if(demand == 0){
			demand = 1;
		}
		if(supply == 0){
			supply = 1;
		}
		var startPrice:double = resource.startPrice();

		relativeDemand = (demand / (eqilibrium + demand));
		relativeSupply = (supply / (eqilibrium + supply));
		var price:int = Util.mathRound(startPrice * (relativeDemand/relativeSupply));
    	return price;
    }

    // Buy product.
    function buyResource(r:int, amount:int, total:int){
        if(player.resourceValue("Guldens")>=  total && (resources[r] as MarketResource).supply() >= amount && (resources[r] as MarketResource).supply() > 0){
            if(networking.isMine()){
                var resource:MarketResource = resources[r] as MarketResource;
                resource.setSupply(resource.supply() - amount);
                buyActualResource(r,amount,total);
            } else {
                sendRPC("RPCBuyResource("+r.ToString()+","+amount+","+total+")",RPCMode.Server);
            }
        }
    }

 	// Change resource values.
    private function buyActualResource(r:int, amount:int,total:int){
        var current:int = player.resourceValue((resources[r] as MarketResource).resourceName());
        var currentgold:int = player.resourceValue("Guldens");
    	player.setResourceValue((resources[r] as MarketResource).resourceName(), current + amount);
    	player.setResourceValue("Guldens", currentgold - total);
    }

	// Change resource value, remote procedure.
    private function buyActualResourceRPC(param:String[],playerID:int){
        var r:int = int.Parse(param[0]);
        var amount:int = int.Parse(param[1]);
        var total:int = int.Parse(param[2]);

        var current:int = player.resourceValue((resources[r] as MarketResource).resourceName());
        var currentgold:int = player.resourceValue("Guldens");
    	player.setResourceValue((resources[r] as MarketResource).resourceName(), current + amount);
    	player.setResourceValue("Guldens", currentgold - total);
    }

 	// Sell resources
    function sellResource(r:int, amount:int, total:int){
    	var resource:MarketResource = resources[r] as MarketResource;
    	resource.setSupply(resource.supply() + amount);
        if(networking.isMine()){
    	    var current:int = player.resourceValue(resource.resourceName());
            var currentgold:int = player.resourceValue("Guldens");
            player.setResourceValue(resource.resourceName(), current - amount);
            player.setResourceValue("Guldens", currentgold + total);
        } else {
            sendRPC("RPCSellResource("+r.ToString()+","+amount+","+total+")",RPCMode.Server);
        }
    }

	// Sell resources, Remote procedure
    private function sellActualResourceRPC(param:String[],playerID:int){
        var r:int = int.Parse(param[0]);
        var amount:int = int.Parse(param[1]);
        var total:int = int.Parse(param[2]);

        var current:int = player.resourceValue((resources[r] as MarketResource).resourceName());
        var currentgold:int = player.resourceValue("Guldens");
    	player.setResourceValue((resources[r] as MarketResource).resourceName(), current - amount);
    	player.setResourceValue("Guldens", currentgold + total);
    }

	// Calculate buying price for x op product.
    function calculateTotalBuyingPrice(r:int, amount:int){
    	var resource:MarketResource = resources[r] as MarketResource;
		var relativeDemand:double = 0;
		var relativeSupply:double = 0;
		var eqilibrium:double = resource.eqilibrium();
		var demand:double = Mathf.Floor(resource.demand());
		var supply:double = Mathf.Floor(resource.supply());
		if(supply == 0){
			supply = 1;
		}
		var tempSupply:double = supply;
		var startPrice:double = resource.startPrice();
		var price:int = 0;
		for(var i:int = supply; i > (supply-amount); i--){
			relativeDemand = (demand / (eqilibrium + demand));
			relativeSupply = (tempSupply / (eqilibrium + tempSupply));
			price += Util.mathRound(startPrice * (relativeDemand/relativeSupply));
			tempSupply--;
		}
		return price;
    }

	// Calculate selling price for x op product.
    function calculateTotalSellingPrice(r:int, amount:int){
    	var resource:MarketResource = resources[r] as MarketResource;
		var relativeDemand:double = 0;
		var relativeSupply:double = 0;
		var eqilibrium:double = resource.eqilibrium();
		var demand:double = Mathf.Floor(resource.demand());
		var supply:double = Mathf.Floor(resource.supply());
		var tempSupply:double = supply;
		var startPrice:double = resource.startPrice();
		var price:int = 0;
		tempSupply++;
		for(var i:int = supply; i > (supply-amount); i--){
			relativeDemand = (demand / (eqilibrium + demand));
			relativeSupply = (tempSupply / (eqilibrium + tempSupply));
			price += Util.mathRound(startPrice * (relativeDemand/relativeSupply));
			tempSupply++;
		}
		return Util.mathRound(price*sellingConstant);
    }

	// Get the market registry's resource map.
    function resourceMap(){
        if(marketRegistry){
    	    return marketRegistry.resourceMap();
    	} else {
    	    return null;
    	}
    }
}
