#pragma strict

public class MarketResource{
	private var _startPrice:int;
	private var _supply:double;
	private var _demand:int;
	private var _currentPrice:int;
	private var _resourceName:String;
	private var _eqilibrium:int;
	private var _demandConstant:double;

	function MarketResource(name:String, startprice:int, eqilibrium:int, quantity:double, demandC:double){
		_supply = quantity;
		_demandConstant = demandC;
		_currentPrice = startprice;
		_startPrice = startprice;
		_resourceName = name;
		_eqilibrium = eqilibrium;
		calculateDemand(Population.size());
	}

	// Calculate demand
	public function calculateDemand(population:int){
		_demand = population*_demandConstant;
	}

	public function currentPrice():int{
		return _currentPrice;
	}

	public function supply():double{
		return _supply;
	}

	public function demand():int{
		return _demand;
	}
	public function demandConstant():double{
	    return  _demandConstant;
	}

	public function startPrice():int{
		return _startPrice;
	}

	public function resourceName():String{
		return _resourceName;
	}

	public function eqilibrium():int{
		return _eqilibrium;
	}

	public function setEqilibrium(value:double){
		_eqilibrium = value;
	}

	public function setCurrentPrice(value:int){
		_currentPrice = value;
	}

	public function setSupply(value:double){
		if(value < 1){
			value = 0;
		}
		_supply = value;
	}

	public function setStartPrice(value:int){
		_startPrice = value;
	}

	public function setResourceName(value:String){
		_resourceName = value;
	}
}
