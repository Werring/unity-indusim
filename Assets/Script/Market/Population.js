public class Population{
	
	private static var count:double = 30.1;
	
	public static function size():double{
		return count;
	}
	
	public static function setSize(val:double){
		count = val;
	}

}
