#pragma strict

public class RegistryMarket{

	private var _resourceMap:Array = null;
	private static var _instance:RegistryMarket = null;

	private function RegistryMarket(){}

	public static function getInstance(){
		if(_instance == null){
			_instance = new RegistryMarket();
		}
		return _instance;
	}

	function resourceMap(){
		return _resourceMap;
	}

	function setResourceMap(resMap:Array){
		_resourceMap = resMap;
	}

}
