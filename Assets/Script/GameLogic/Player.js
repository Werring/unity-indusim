#pragma strict

public class Player {

    private static var _instance:Player = null;
    private var registry:RegistryMarket = RegistryMarket.getInstance();
    private var resourceCount:Hashtable = new Hashtable();
    private var _init:System.Boolean = false;
    private var _name:String = null;

    private function Player(){

    }

    public static function getInstance():Player{
        if(_instance == null){
            _instance = new Player();
        }
        return _instance;
    }

	// Return initialized
    public function isInit():boolean{
        return _init;
    }

	// Initialize
    public function init(){
        _init = true;
        Debug.Log("Init player");
    }

	// Get resource value
    public function resourceValue(name:String):double{
        return (resourceCount[name] == null) ? 0.0 : double.Parse(resourceCount[name].ToString());
    }

	// Set resource value
    public function setResourceValue(name:String, amount:double){
        resourceCount[name] = amount;
    }

	// Get player name.
    public function get name():String{
        if(_name == null){
            try {
            var uid:int = (GameObject.Find("NetworkScripts").GetComponent(Networking) as Networking).userID;
            _name = "Player " + uid;
            } catch(e:System.Exception) {

            }
        }
        return _name || "test";
    }

	// Set player name.
    public function set name(value:String){
        _name = value;
    }

	// Get stat file name
    public function get statfilename():String{
        return "StatData_" + Player.getInstance().name + ".indusim";
    }
}
