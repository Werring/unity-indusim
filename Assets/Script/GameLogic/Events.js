#pragma strict

//import System.IO;
import LitJson;

class Events extends NetworkBehaviour {
	public static var instance : Events;
	
	var ShowEventText : UILabel;
	var tutorial : GameObject;	//tutorial what do all the buttons do.
	//var sendMessage : GameObject;//send a message
	var mission : GameObject;	//missions like bring ... get ...
	var invention : GameObject;	//inventions raw information about inventions and speed up for certain productions
	//var message : GameObject;	//messages from other players
	//var older : UIPopupList;		//popup list with all the events that have happend
	//pop up list only has to remember text and maybe titel

	var closeButton : GameObject;
	var actionButton : GameObject;

	//don't set in scene
	var eventList = new Array();
	//var eventHappened = new Array(); //for the popup list
	var tutorialLabel : UILabel;
	var missionLabel : UILabel;
	var inventionLabel : UILabel;
	//var messageLabel : UILabel;
	var actionButtonLabel : UILabel;

	//dit werkt niet zonder static. klopt de awake wel
	static var tutorialInformation : EventInformation = null;
	static var missionInformation : EventInformation = null;
	static var inventionInformation : EventInformation = null;
	
	var tutorialSeen : boolean = true;
	var missionSeen : boolean = true;
	var inventionSeen : boolean = true;
	//var messageInformation : EventInformation = null;
	
	var newEventSound : AudioClip;
	var audioSource : AudioSource;

	private var firstMission : boolean = false;

	// Initialize events.
	function Start () {
		GetEvents();
		instance = this;
		tutorialEvent("begin");
	}

	// Invention
	function inventionEvent(year : int) {//inventions, maybe call from timeline
		for(var i = 0; i < eventList.length; i++){
			var event = eventList[i] as EventInformation;
			if(event.type == "invention" && int.Parse(event.requirement) == year) {//inventions
				//change text color on the button
				inventionLabel.color = Color.green;
				inventionInformation = event;
				//older.items.Insert(0, "1");//add to popup list
				inventionSeen = false;
				audioSource.PlayOneShot(newEventSound);
				break;
			}
		}
	}

	// Tutorial from json
	function tutorialEvent(name : String) {
		for(var i = 0; i < eventList.length; i++){
			var event = eventList[i] as EventInformation;
			if(event.type == "tutorial" && event.requirement == name){//tutorials
				//change text color on the button
				tutorialLabel.color = Color.green;
				tutorialInformation = event;
				tutorialSeen = false;
				audioSource.PlayOneShot(newEventSound);
				break;
			}
		}
	}

	// Tutorial from text
	function newTutorialEvent(text:String) {
		var info = new EventInformation(text, Resource("null", 0, 0.0), Resource("null", 0, 0.0), "tutorial", "added");
		//eventList.push(info);
		tutorialLabel.color = Color.green;
		tutorialInformation = info;
		tutorialSeen = false;
		audioSource.PlayOneShot(newEventSound);
	}

	/*function messageEvent(text : String) {
		messageLabel.color = Color.green;
		messageInformation = new EventInformation(text, null, null, "message", "");
	}*/

	// Mission.
	function missionEvent(number : String) {//random number every year
		for(var i = 0; i < eventList.length; i++){
			var event = eventList[i] as EventInformation;
			if(event.type == "mission" && event.requirement == number){//mission
				//change text color on the button
				missionLabel.color = Color.green;
				missionInformation = event;
				if(!firstMission){
					firstMission = true;
					tutorialEvent("first mission");
				}
				missionSeen = false;
				audioSource.PlayOneShot(newEventSound);
				break;
			}
		}
	}

	// Count events per type.
	function countEventType(eventType : String) {
		var counter = 0;
		for(var i = 0; i < eventList.length; i++){
			var event = eventList[i] as EventInformation;
			if(event.type == eventType){
				counter++;
			}
		}
		return counter;
	}

	// Stop mission event.
	function stopMissionEvent() {
		missionInformation = null;
		missionLabel.color = Color.white;
		missionSeen = true;
	}

	// Get events from Json
	function GetEvents () {
		//read events from jason
		//make eventinformation objects
		//keep information is list
		var data:JsonData = JsonMapper.ToObject(Resources.Load("JSON/Events").ToString());
		for(var i = 0; i < data.Count; i++){
			var da:JsonData = data[i];
			var info = new EventInformation(da["text"].ToString(),
				Resource(toArray(da, "reward")),
				Resource(toArray(da, "mission")),
				da["requirement"]["type"].ToString(), da["requirement"]["when"].ToString());
			eventList.push(info);
		}
	}

	// read JsonData to array
	function toArray(building:JsonData, item:String):Array{
		for(var i = 0; i < building[item].Count; i++){
			var currentItem:JsonData = building[item][i];
			if(currentItem["type"].ToString() == ""){
				Debug.LogError('Item ' + currentItem["type"] + 'is empty.');
			}else{
				return new Array(
					currentItem["type"].ToString(),
					int.Parse(currentItem["amount"].ToString())
				);
			}
		}
		return null;
	}

	// Init
	function Awake(){
		Debug.Log("events awake");
        if(GameObject.Find("/_globalScripts")){
            var globalBuildingScripts:GameObject = GameObject.Find("/_globalScripts/Events");
            if(globalBuildingScripts == null){
                this.gameObject.transform.parent = GameObject.Find("/_globalScripts").transform;
                DontDestroyOnLoad(this.gameObject);
                instance = this;
            } else {
                Destroy(this.gameObject);
            }
        }
	}

	// Initialize if the current scene is the game.
	function OnLevelWasLoaded(level:int){
		if(level == 1){
			instantiate();
		}	
	}

	// Initialize
	function instantiate(){
		Debug.Log("events get buttons");
		//zit de tweede keer een map verder namelijk in de scriptholder
		ShowEventText	= GameObject.Find("eventLabel").GetComponent(UILabel);
		tutorial		= GameObject.Find("tutorialButton");
		mission			= GameObject.Find("missionButton");
		invention		= GameObject.Find("inventionButton");
		closeButton		= GameObject.Find("closeButton");
		actionButton	= GameObject.Find("actionButton");
		
		audioSource		= GameObject.Find("EyeSocket/Eye").GetComponent(AudioSource);
		
		//message
		//older
		//sendMessage
	
		//set the functions the buttons must call.
		UIEventListener.Get(tutorial).onClick = showTutorial;
		UIEventListener.Get(mission).onClick = showMission;
		UIEventListener.Get(invention).onClick = showInvention;
		//UIEventListener.Get(message).onClick = showMessage;
		UIEventListener.Get(closeButton).onClick = close;
		UIEventListener.Get(actionButton).onClick = close;
		//UIEventListener.Get(sendMessage).onClick = showSendMessage;

		tutorialLabel = tutorial.GetComponentInChildren(UILabel);
		missionLabel = mission.GetComponentInChildren(UILabel);
		inventionLabel = invention.GetComponentInChildren(UILabel);
		//messageLabel = message.GetComponentInChildren(UILabel);
		actionButtonLabel = actionButton.GetComponentInChildren(UILabel);
		
		if(!tutorialSeen){
			tutorialLabel.color = Color.green;
		}
		if(!missionSeen){
			missionLabel.color = Color.green;
		}
		if(!inventionSeen){
			inventionLabel.color = Color.green;
		}
	}

	//buttons clicked
	function showTutorial(){
		Debug.Log("tutorial clicked");
		tutorialLabel.color = Color.white;
		ShowEventText.text = tutorialInformation.text;
		actionButtonLabel.text = "ok";
		tutorialSeen = true;
	}

	/*function showSendMessage(){
		Debug.Log("show send message clicked");
		UIEventListener.Get(actionButton).onClick = sendTheMessage;
		actionButtonLabel.text = "verstuur";
	}*/

	// Display mission info
	function showMission(){
		Debug.Log("mission clicked");
		missionLabel.color = Color.white;
		missionSeen = true;
		if(missionInformation != null){
			ShowEventText.text = missionInformation.text;
			actionButtonLabel.text = "voltooi";
			UIEventListener.Get(actionButton).onClick = finishMission;
		}else{
			ShowEventText.text = " ";
			actionButtonLabel.text = "ok";
			UIEventListener.Get(actionButton).onClick = close;
		}
	}

	// Display invention
	function showInvention(){
		Debug.Log("invention clicked");
		inventionLabel.color = Color.white;
		ShowEventText.text = inventionInformation.text;
		actionButtonLabel.text = "ok";
		inventionSeen = true;
	}

	/*function showMessage(){
		Debug.Log("message clicked");
		messageLabel.color = Color.white;
		ShowEventText.text = messageInformation.text;
	}*/

	// Close popup
	function close(){
		Debug.Log("close");
		UIEventListener.Get(actionButton).onClick = close;
		actionButtonLabel.text = "ok";
	}

	/*function sendTheMessage(){
		Debug.Log("message send");
		UIEventListener.Get(actionButton).onClick = close;
	}*/

	// Turn in mission
	function finishMission(){
		//if the events has a mission with a type null it gives problems
		UIEventListener.Get(actionButton).onClick = close;
		if(missionInformation != null){
			var player:Player = Player.getInstance();
			var playerResource:int = player.resourceValue(missionInformation.mission.resourceName);
			if(missionInformation.mission.amount > playerResource){//not enough
				Debug.Log("not enough resources to complete this mission");
				return;
			}
			var addResource:double = missionInformation.mission.amount;
			Debug.Log(missionInformation.mission.resourceName + " " + addResource);
			player.setResourceValue(missionInformation.mission.resourceName,playerResource - addResource);
			addResource = missionInformation.reward.amount;
			playerResource = player.resourceValue(missionInformation.reward.resourceName);
			player.setResourceValue(missionInformation.reward.resourceName,playerResource + addResource);
			//send mission done
			missionInformation = null;
			ShowEventText.text = "";
			Debug.Log("finished mission");
			close();
			return;
		}
		Debug.Log("no mission");
	}
}
