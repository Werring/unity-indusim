#pragma strict
class Resource {
	public var resourceName:String;
	public var amount:int;
	public var production:float;
	
	function Resource(name:String, amount:int, production:float){
		this.resourceName = name;
		this.amount = amount;
		this.production = production;
	}

	function Resource(values:Array){
		this.resourceName = values[0].ToString();
		this.amount = int.Parse(values[1].ToString());
		this.production = (values.length > 2) ? float.Parse(values[2].ToString()) : 0.0;
	}
}
