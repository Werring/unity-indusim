#pragma strict

// Even info holder.
class EventInformation {
	var text:String;
	var reward:Resource;
	var mission:Resource;
	var type:String;
	var requirement:String;
	
	function EventInformation (text:String, reward:Resource, mission:Resource, type:String, requirement:String) {
		this.text = text;
		this.reward = reward;
		this.mission = mission;
		this.type = type;
		this.requirement = requirement;
	}
}
