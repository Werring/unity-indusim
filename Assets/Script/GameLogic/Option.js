#pragma strict

class Option {
	var optionName : String;
	var optionCode : String;
	var tooltipText : String;
	var story : String;
	var available : boolean;
}