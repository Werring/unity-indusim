#pragma strict

class GridDisplay extends MonoBehaviour {
	var gridOverlay : Light;

	// Toggle grid if [G] is pressed
	function Update () {
		if (Input.GetKeyUp(KeyCode.G)) {
			gridOverlay.enabled = !gridOverlay.enabled;
		}
	}
}