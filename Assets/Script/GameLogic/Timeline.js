#pragma strict
class Timeline extends NetworkBehaviour
{
    public static var instance:Timeline;
    var time : int = 1700;

	public static var playtime : int = 1;
	private var timelineFiller:TimeLineMover;
	private var events : Events;

	// Initialize
	function Awake(){
        if(GameObject.Find("/_globalScripts")){
            var globalBuildingScripts:GameObject = GameObject.Find("/_globalScripts/Timeline");
            if(globalBuildingScripts == null){
                this.gameObject.transform.parent = GameObject.Find("/_globalScripts").transform;
                DontDestroyOnLoad(this.gameObject);
                instance = this;
            } else {
                Destroy(this.gameObject);
            }
        }
	}

	// Start
	function Start(){
	    super.Start();
		registerStateSync();

		InvokeRepeating("AdjustTime", 1, (0.3*playtime));
		sync();
    }

	// Sync timeline with host.
	function OnSerializeNetworkView(stream:BitStream,info:NetworkMessageInfo) {
		var tmpTime:int;
		if(stream.isWriting)
		    tmpTime=time;

		stream.Serialize(tmpTime);

		if(stream.isReading)
		    time = tmpTime;

		sync();
	}

	// Sync timeline display with value.
    function sync(){
        var i : float = (time - 1700)/200.0;
		if(timelineFiller == null){
		    timelineFiller = TimeLineMover.instance;
		}
		timelineFiller.syncTimeline(i);
    }


	// Time passes by.
    function AdjustTime() {
		time++;
        if(time == 1900){
            Application.LoadLevel(3);
        }
		if(events == null){
			events = Events.instance;
		}
		//events
		events.inventionEvent(time);
		var ran = Random.Range(0, 20);//tested every year so there should be a mission once every X years
		if(ran == 1)
		{
			ran = Random.Range(0, events.countEventType("mission"));//should remember the number of missions
			events.missionEvent(ran.ToString());
		}
	}
}
