#pragma strict

class TimeLineMover extends MonoBehaviour {
	var timeline : GameObject;

	static var instance:TimeLineMover;
	private var timelinefill : UIFilledSprite = null;
	private var gametime : int;

	// Initialize
	function Start () {
	    instance = this;
		InvokeRepeating("fillBar", 0, 0.01);

		timelinefill = timeline.GetComponent(UIFilledSprite);
		timelinefill.fillAmount = 0;
		if(Timeline.instance) {
	        Timeline.instance.sync();
		}
	}

	// Add time to the bar.
	function fillBar() {
		timelinefill.fillAmount += ((1)/20000.0)/(0.3*Timeline.playtime);
	}

	// Sync value.
	function syncTimeline(sync:float){
        timelinefill.fillAmount = sync;
	}


}
