#pragma strict

class ResourceMapper extends MonoBehaviour {
	var terrain: Terrain;
	var cam: Camera;
	var layerMask: LayerMask;
	var gridX : float = 50;
	var gridY : float = 50;
	var gridZ : float = 50;

	var isLoaded = false;

	// The origin of the sampled area in the plane.
	var scale: float = 5.0;
	private var w:float;
	private var h:float;

	private static var GRASS:int  = 0;
	private static var DESERT:int = 1;
	private static var ROCK:int   = 2;
	private static var GREEN:int  = 3;

	private static var resourceMap:int = 0;
	private static var textureMap:int = 1;

	private static var resourceTreshold:double = 0.6; // Treshold for resources being available or not (In green overlay)

	private var terrainData:TerrainData;
	private var defaultAlphaMap: float[,,];

	private static var resources:Hashtable; 
	private static var offsetsX:Hashtable = new Hashtable();
	private static var offsetsZ:Hashtable = new Hashtable();

	private var done = false;

	// Initialize
    function Awake(){
        if(resources == null){
            resources = new Hashtable();
        }
    }

	function Start () {
		if(!isLoaded){
			terrainData = terrain.terrainData;

			w = terrainData.alphamapWidth;
			h = terrainData.alphamapHeight;

			defaultAlphaMap = terrainData.GetAlphamaps(0, 0, w, h);
			for (var x:float = 0.0; x < w; x++) {
				for (var z:float = 0.0; z < h; z++) {
					defaultAlphaMap[x, z, GREEN] = 0f;
				}
			}

			hideResources();
			isLoaded = true;
		}

	}

	function Update () {
	    var ray: Ray = cam.ScreenPointToRay(Input.mousePosition);
		var hit: RaycastHit;
		if(Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, layerMask)) {
			var x:int = Mathf.Floor(hit.point.x);
			var z:int = Mathf.Floor(hit.point.z);
		}
	}

	function OnGUI () {
	 	//GUI.backgroundColor = Color.black;
	    //GUI.Label(Rect(10, 50, 200, 20), coalValue);
	}

	function addResource(type:String) {
		if(resources.ContainsKey(type)){
		    return false;
		}
		terrainData = terrain.terrainData;

		resources.Add(type, new Hashtable());
		var resMap:Array = new Array();
		var texMap:float[,,] = terrainData.GetAlphamaps(0, 0, w, h);

		var offsetX:float = (Random.value * w * 50);
		var offsetZ:float = (Random.value * h * 50);
		offsetsX.Add(type, offsetX);
		offsetsZ.Add(type, offsetZ);
		
		for (var x:float = 0; x < w; x++) {
			for (var z:float = 0; z < h; z++) {
			  
			 	var sample:float = perlin(type, x, z);
				texMap[x, z, GREEN] = sample;
			}
		}

		(resources[type] as Hashtable).Add(textureMap, texMap);
		(resources[type] as Hashtable).Add("enabled", false); 
		
		return true;
	} 
	
	function perlin(type:String, x:float, z:float){
		var xCoord:float = float.Parse(offsetsX[type].ToString()) + Mathf.Floor(x) / w * scale;
		var zCoord:float = float.Parse(offsetsZ[type].ToString()) + Mathf.Floor(z) / h * scale;
	 	var sample:float = Mathf.PerlinNoise(xCoord, zCoord);
		return (sample >= resourceTreshold) ? 1 : 0;
	}

	function ToggleResource(type:String){
		if((resources[type] as Hashtable)["enabled"] == true){
			hideResources();
		}else{
			showResource(type);
		}
	}

	function showResource(type:String){
		if(type != "" && (resources[type] as Hashtable)["enabled"] == false){
			hideResources();
			terrainData.SetAlphamaps(0, 0, ((resources[type] as Hashtable)[textureMap] as float[,,]));
			(resources[type] as Hashtable)["enabled"] = true;
		}
	}

	function hideResources(){
		terrainData.SetAlphamaps(0, 0, defaultAlphaMap);
		for(var type:Object in resources.Keys){
			(resources[type] as Hashtable)["enabled"] = false;
		}
	}

	function SnapGrid(vec : Vector3) : Vector3 {
		var pos : Vector3;
		pos.x = (Mathf.Floor(vec.x / gridX) * gridX);
		pos.y = vec.y;
		//pos.y = Mathf.Round(vec.y / gridY) * gridY;
		pos.z = (Mathf.Floor(vec.z / gridZ) * gridZ);
		return pos;
	}

}
