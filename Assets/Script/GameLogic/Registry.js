#pragma strict

import LitJson;

class Registry extends MonoBehaviour {
    private static var importedJSON:boolean = false;
	//pointers opslaan
	var technology = new Array();
	var options = new Array();
	var buildings = new Array();
	static var resources = new Array();
	static var buildingTemplates = new Array();
	static var productions = new Hashtable ({"Vlees": 2.0, "Granen": 2.0, "Brood": 1.0, "Suikerbiet": 4.0});
	var grid:Grid;
	var player:Player;

	var selectedBuilding:Building;
	var buildingDistance:float = 99999;

	var resourceMapper:ResourceMapper;

	// Initialize, read JSON files.
	function Start() {
		resourceMapper = GameObject.Find("_sceneScripts").GetComponent(ResourceMapper);
		if(!resourceMapper.isLoaded){
			resourceMapper.Start();
		}
        player = Player.getInstance();

        if(!importedJSON){
            //resources
            var resource:JsonData = JsonMapper.ToObject(Resources.Load("JSON/Resource").ToString());
            for(var i = 0; i < resource.Count; i++){
                var re = resource[i];
                addResource(
                    new Resource(
                        re["name"].ToString(),
                        double.Parse(re["amount"].ToString()),
                        0.0
                    )
                );
            }

            //building templates
            var buildingTemplate:JsonData = JsonMapper.ToObject(Resources.Load("JSON/BuildingTemplate").ToString());
            for(var j = 0; j < buildingTemplate.Count; j++){
                var building:JsonData = buildingTemplate[j];
                var prefab:GameObject = Resources.Load("Prefab/" + building["name"]) as GameObject;
                var dragger:GameObject = Resources.Load("Prefab/" + building["name"] + "Dragger") as GameObject;
                var cost:Array = toNameValueArray(building, "cost");


                var requiredResources:String = building["resourceRequired"].ToString();
                if(requiredResources != ""){
                    resourceMapper.addResource(requiredResources);
                }
                if(cost.length > 0){
                    addBuildingTemplate(
                        new BuildingTemplate(
                            building["name"].ToString(),
                            building["displayName"].ToString(),
                            building["description"].ToString(),
                            int.Parse(building["size"]["x"].ToString()),
                            int.Parse(building["size"]["z"].ToString()),
                            prefab,
                            dragger,
                            cost,
                            toNameValueArray(building, "produce"),
                            toNameValueArray(building, "require"),
                            requiredResources,
							toArray(building, "influencedBy"),
							toArray(building, "influences")
                        )
                    );
                }
            }
            importedJSON = true;
		}
	}

	/*
	// Technologies, not yet implemented.
	function addTechnology (t : Technology) {
		technology.Push(t);
	}

	function getTechnology (name : String) {
		var temp : Technology;
		for(var i = 0; i < technology.length; i++)
		{
			temp = technology[i] as Technology;
			if(temp.technologyName.Equals(name))
				return temp as Technology;
		}
		return null;
	}

	function addOption (o : Option) {
		options.Push(o);
	}

	// Options, not yet implemented.
	function getOption (name : String) {
		var temp : Option;
		for(var i = 0; i < options.length; i++)
		{
			temp = options[i] as Option;
			if(temp.optionName.Equals(name))
				return temp as Option;
		}
		return null;
	}
	*/

	// Add a building to the available buildings list.
	function addBuilding (b : Building) {
		buildings.Push(b);
	}

	// Get building by position
	function getBuilding(position:Vector2):Building{
		var temp : Building;
		for(var i = 0; i < buildings.length; i++)
		{
			temp = buildings[i] as Building;
			if(temp.position == position)
				return temp as Building;
		}
		return null;
	}

	// Add a resource to the available resources list.
	function addResource (r : Resource) {
	    player.setResourceValue(r.resourceName,r.amount);
		resources.Push(r);
	}

	// Get a resource by name
	function getResource(name:String) {
		var temp:Resource;
		for(var i = 0; i < resources.length; i++){
			temp = resources[i] as Resource;
			if(temp.resourceName.Equals(name))
				return temp as Resource;
		}
		return null;
	}

	// Add a building template.
	function addBuildingTemplate (bt : BuildingTemplate) {
		buildingTemplates.Push(bt);
	}

	// Get a building template by name.
	function getBuildingTemplate(name : String) {
		var temp : BuildingTemplate;
		for(var i = 0; i < buildingTemplates.length; i++)
		{
			temp = buildingTemplates[i] as BuildingTemplate;
			if(temp.buildingTemplateName.Equals(name))
				return temp as BuildingTemplate;
		}
		return null;
	}

	// Read JsonData to Associative Array.
	function toNameValueArray(building:JsonData, item:String){
		var output = new Array();

		for(var i = 0; i < building[item].Count; i++){
			var currentItem:JsonData = building[item][i];
			if(currentItem["value"].ToString() == ""){
				Debug.LogError('Missing value for ' + item + ': "' + currentItem["name"] + '" of building "' + building["name"] + '". Skipping Building.');
			}else{
				output.push(new Array(
						currentItem["name"].ToString(),
						double.Parse(currentItem["value"].ToString()),
						0.0
					)
				);
			}
		}
		return output;
	}

	// Read JsonData to Array.
	function toArray(building:JsonData, item:String){
		var output = new Array();
		for(var i = 0; i < building[item].Count; i++){
			output.push(building[item][i]);
		}
		return output;
	}

	// Set building as clicked.
	function setClicked(building:Building){
		selectedBuilding = building;
	}
}
