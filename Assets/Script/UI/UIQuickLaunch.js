#pragma strict

class UIQuickLaunch extends MonoBehaviour {
	static var loading_on : boolean = false;
    var Loading_Screen:Texture2D;
    
	var myFunc = function(go : GameObject) {
		loading_on = true;
		Application.LoadLevel(1);
	};
	
	function Start() {
		UIEventListener.Get(this.gameObject).onClick = myFunc;
	}
     
    function OnGUI () {
	    if(loading_on){
	    	var GameData : Vector2 = new Vector2(1, 1);
		    GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3(1.0*Screen.width/GameData.x, 1.0*Screen.height/GameData.y, 1.0));
		    GUI.depth =-10;
		    GUI.Box(new Rect(0,0,1024,768),Loading_Screen);
	    }
    }
}