#pragma strict

class ResourceListener extends MonoBehaviour {
	private var thisPlayer : Player;
	private var amount : double = 0.0;
	private var reslabel : UILabel;
	var resourceName : String;
	
	// sets variables
	function Start() {
		thisPlayer = Player.getInstance();
		reslabel = this.gameObject.GetComponent(UILabel);
	}
	
	// updates the label to the current resource value
	function Update() {
		if(amount != thisPlayer.resourceValue(resourceName)) {
			amount = thisPlayer.resourceValue(resourceName);
			reslabel.text = Util.thousandsSeparator(amount.ToString());
		}
	}
}