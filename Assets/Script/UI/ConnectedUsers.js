#pragma strict
class ConnectedUsers extends NetworkBehaviour {
    var players:Players;

    var oldLength:int;

    var started:boolean = false;

	// Register's the RPC
    function Start(){
        super.Start();
        players = (GameObject.Find("NetworkScripts").GetComponent(Players) as Players);
        registerRPC("startGame",startGame);
    }

	// Creates an Unity default GUI for hosting/joining
    function OnGUI(){
        var playerList:Hashtable = players.playerList;
        if(oldLength != Network.connections.length){
            print(Network.connections.length);
            oldLength = Network.connections.length;
        }
        var i:int = 0;
        for(var key:Object in playerList.Keys){
            var player:String = playerList[key] as String;
            GUI.Label(Rect(Screen.width/2-75, 100+(i*20),150,30), player + "  (" + key + ")");
            i++;
        }
        if(networking.isMine() && !started && i > 0){
            if(GUI.Button(Rect(Screen.width/2-75, 100+(i*20),150,30),"Start")){
                sendRPC("startGame",RPCMode.All);
                started = true;
            }
        }
    }

	// Start the game
    function startGame(){
        Application.LoadLevel(1);
    }
}