#pragma strict

class BuildingButtonHandler extends MonoBehaviour {
	private var bp : BuildingPlacer;
	private var registry: Registry;
	var placer : BuildingPlacer.Place;

	// Initialize variables
	function Start () {
		var go : GameObject;
		go = GameObject.Find("_sceneScripts");
		
	    registry = go.GetComponent(Registry);
		bp = go.GetComponent(BuildingPlacer);
		
		UIEventListener.Get(this.gameObject).onClick = buildFunc;
		UIEventListener.Get(this.gameObject).onHover = tooltipFunc;
	}
	
	// Binds an building to the mouse
	var buildFunc = function(go : GameObject) {
		bp.place = placer;
		bp.BindBuilding();
	};
	
	// Generates an tooltip for building
	var tooltipFunc = function(go : GameObject, isOver : boolean) {
		if(isOver) {
			var template : BuildingTemplate = registry.getBuildingTemplate(placer.ToString());
			var tt : String = "";
			tt += "[bed8fe]" + template.displayName + "\n";
			tt += "[ffffff]-------------------\n";
			tt += "[ffffff]Kosten:\n";
			for(var iCost = 0; iCost < template.cost.length; iCost++){
				var resourceCost : Resource = new Resource(template.cost[iCost]);
				var sCost : String =  resourceCost.amount + "x " +resourceCost.resourceName;
				tt += sCost + "\n";
			}
			
			if(template.produce.length > 0) {
				tt += "[ffffff]-------------------\n";
				tt += "[00ff00]Productie:\n";
				for(var i = 0; i < template.produce.length; i++){
					var resourceProduce : Resource = new Resource(template.produce[i]);
					var s : String =  resourceProduce.amount + "x " +resourceProduce.resourceName;
					tt += s + "\n";
				}
				
				
				if(template.require.length > 0) {
					tt += "\n[ff0000]Verbruik:\n";
				for(var i2 = 0; i2 < template.require.length; i2++){
					var resourceReq : Resource = new Resource(template.require[i2]);
					var s2 : String =  resourceReq.amount + "x " +resourceReq.resourceName;
					tt += s2 + "\n";
				}
				}
				tt += "[ffffff]-------------------\n";
			}
			UITooltip.ShowText(tt);
		} else
			UITooltip.ShowText(null);
	};
}