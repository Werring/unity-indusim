#pragma strict

class ResourceTooltip extends MonoBehaviour {
	var resName : String;
	var resDesc : String;
	var resPath : String;
	var registry : Registry;

	// Binds function to onHover
	function Start () {
	    registry			= GameObject.Find("_sceneScripts").GetComponent(Registry);
		UIEventListener.Get(this.gameObject).onHover = tooltipFunc;
	}
	
	// Generates tooltip text for the resource
	var tooltipFunc = function(go : GameObject, isOver : boolean) {
		if(isOver) {
			var tt : String = resName;
			if(resDesc != null && resDesc != "") {
				tt += " ("+resDesc+")";
			}
			
			if(resPath != null && resPath != "") {
				tt += "\n-------------------\n";
				tt += resPath;
			}
			
	        if(registry.productions != null && (registry.productions as Hashtable)[resName]){
	        	tt += "\n-------------------\n";
	        	tt += "Productie:\n";
	        	tt += Util.thousandsSeparator((registry.productions as Hashtable)[resName]) + " per " + Building.productionTime + "s";
	        }
			
			UITooltip.ShowText(tt);
		} else
			UITooltip.ShowText(null);
	};
}