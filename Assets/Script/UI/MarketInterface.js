#pragma strict

class MarketInterface extends NetworkBehaviour
{
	var market:Market;
	var marketBuy:GameObject;
	var marketSell:GameObject;
	var toggleBuyImage:GameObject;
	var toggleSellImage:GameObject;
	var player:Player = Player.getInstance();
	var initialized:boolean = false;
	var registry:RegistryMarket;
	var resourceList:Array;
	var totalPriceList:Array = new Array();
	var importPriceList:Array = new Array();
	
	// Sets references and variables
	function Start(){
		marketBuy = GameObject.Find("BuyMarket");
		marketSell = GameObject.Find("SellMarket");
		toggleBuyImage = GameObject.Find("BuyToggleDown");
		toggleSellImage = GameObject.Find("SellToggleDown");
		toggleSellImage.SetActive(false);
		UIEventListener.Get(GameObject.Find("BuyToggle")).onPress = buyToggle;
		UIEventListener.Get(GameObject.Find("SellToggle")).onPress = sellToggle;
		UIEventListener.Get(GameObject.Find("LeaveButton")).onPress = leaveMarket;

		registry = RegistryMarket.getInstance() as RegistryMarket;
		market = GameObject.Find("_market").GetComponent(Market) as Market;
    	resourceList = market.resourceMap() as Array;

    	initializeUI();
		marketSell.SetActive(false);
    	initialized = true;
	}
	
	// Updates labels
	function Update(){
		if(initialized){
			market.update();
		}
		updateLabels();
	}
	
	// Goes back to the Game Scene
	function leaveMarket(){
		Application.LoadLevel(1);
	}
	
	// Toggles the Buy interface
	function buyToggle(){
		if(!marketBuy.activeSelf){
			toggleSellImage.SetActive(false);
			toggleBuyImage.SetActive(true);
			marketBuy.SetActive(true);
			marketSell.SetActive(false);
			onToggle();
		}
	}
	
	// Toggles the Sell interface
	function sellToggle(){
		if(!marketSell.activeSelf){
			toggleSellImage.SetActive(true);
			toggleBuyImage.SetActive(false);
			marketSell.SetActive(true);
			marketBuy.SetActive(false);
			onToggle();
		}
	}
	
	// sets all input labels to 1
	function onToggle(){
		for(var i:int = 0; i < resourceList.length; i++){
			totalPriceList[i] = "";
			importPriceList[i] = "";
			if(marketBuy.activeSelf){
				var buyInput:GameObject = GameObject.Find("BuyInput" + i);
				(buyInput.GetComponent(UIInput) as UIInput).text = "1";
			}
			if(marketSell.activeSelf){
				var sellInput:GameObject = GameObject.Find("SellInput" + i);
				(sellInput.GetComponent(UIInput) as UIInput).text = "1";
			}
		}
	}
	
	// binds the Buy and Sell button at end of row
	function initializeUI(){
		while(resourceList == null){
			registry = RegistryMarket.getInstance() as RegistryMarket;
			market = GameObject.Find("_market").GetComponent(Market) as Market;
			resourceList = market.resourceMap() as Array;
			yield WaitForSeconds(0.1);
		}
	
		for(var i:int = 0; i < resourceList.length; i++){
			totalPriceList[i] = "";
			importPriceList[i] = "";
			UIEventListener.Get(GameObject.Find("BuyButton" + i)).onClick = buyRes;
			UIEventListener.Get(GameObject.Find("SellButton" + i)).onClick = sellRes;
		}
	}
	
	// Buys an resource from row e
	function buyRes(e : GameObject){
		var resourceIndex:int = int.Parse(Regex.Replace(e.ToString(), "[^0-9]", ""));
		var buyInput:GameObject = GameObject.Find("BuyInput" + resourceIndex);
		var amount:int;
		var resource:MarketResource = resourceList[resourceIndex] as MarketResource;
		int.TryParse(buyInput.GetComponentInChildren(UILabel).text, amount);
		if(amount > 0){
			if(market.calculateTotalBuyingPrice(resourceIndex, amount) <= player.resourceValue("Guldens")){
				if(resource.supply() >= amount){
					market.buyResource(resourceIndex, amount, market.calculateTotalBuyingPrice(resourceIndex, amount));
				}
			}
		}
	}
	
	// Sells an resource from row e
	function sellRes(e : GameObject){
		var resourceIndex:int = int.Parse(Regex.Replace(e.ToString(), "[^0-9]", ""));
		var sellInput:GameObject = GameObject.Find("SellInput" + resourceIndex);
		var resource:MarketResource = resourceList[resourceIndex] as MarketResource;
		var amount:int;
		int.TryParse(sellInput.GetComponentInChildren(UILabel).text, amount);
		if(amount > 0){
			if(amount <= player.resourceValue(resource.resourceName())){
				market.sellResource(resourceIndex, amount, market.calculateTotalSellingPrice(resourceIndex, amount));
			}
		}
	}
	
	// Updates the labels in the Market UI
	function updateLabels(){
		if(!initialized){
			return;
		}
		resourceList = market.resourceMap();

		//updating gold
		editLabel("Guldens", Mathf.Floor(player.resourceValue("Guldens")).ToString());
		editLabel("PopulationLabel", "Inwoners: " + Mathf.Floor(Population.size()).ToString());
		//looping through all marketResource objects.
		for(var i:int = 0; i < resourceList.length; i++){
			var marketResource:MarketResource = resourceList[i] as MarketResource;
			if(marketBuy.activeSelf){
				var buyInput:GameObject = GameObject.Find("BuyInput" + i);
			}else{
				var sellInput:GameObject = GameObject.Find("SellInput" + i);
			}

			// updating player resources.
			var playerResource:double = player.resourceValue((resourceList[i] as MarketResource).resourceName());
			editLabel("PlayerResource" + i, Mathf.Floor(playerResource).ToString());
			// updating names
			editLabel("ResourceName" + i, marketResource.resourceName());
			// updating quantity
			var quantity:int = Mathf.Floor(marketResource.supply());
			editLabel("Quantity" + i, quantity.ToString());

			var UIInputComponent:UIInput;
			var inputQuantity:String;
			var input:int;
			var totalPrice:String;

			if(marketBuy.activeSelf){
				// updating individual prices
				var importPrice:String = market.getImportPrice(i, false).ToString();
				editLabel("IndividualBuyingPrice" + i, importPrice);
				// updating total
				UIInputComponent = buyInput.GetComponentInChildren(UIInput);
				var labelText:String = buyInput.GetComponentInChildren(UILabel).text;
				inputQuantity = Regex.Replace(buyInput.GetComponentInChildren(UILabel).text, "[^0-9]", "");
				if(UIInputComponent.text != ""){
					UIInputComponent.text = inputQuantity;
				}
				if(!int.TryParse(inputQuantity, input)){
					inputQuantity == "0";
				}
				if(quantity < input){
					if(quantity == 0){
						quantity = 1;
					}
					UIInputComponent.text = quantity.ToString();
					input = quantity;
				}
				totalPrice = market.calculateTotalBuyingPrice(i, input).ToString();
				if(importPriceList[i] != importPrice || totalPrice != totalPriceList[i]){
					totalPriceList[i] = totalPrice;
					editLabel("TotalBuyingPrice" + i, totalPrice);
				}
			}else{
				// updating individual prices
				var exportPrice:String = market.getExportPrice(i).ToString();
				editLabel("IndividualSellingPrice" + i, exportPrice);
				// updating total
				UIInputComponent = sellInput.GetComponentInChildren(UIInput);
				inputQuantity = Regex.Replace(sellInput.GetComponentInChildren(UILabel).text, "[^0-9]", "");
				if(UIInputComponent.text != ""){
					UIInputComponent.text = inputQuantity;
				}
				if(!int.TryParse(inputQuantity, input)){
					inputQuantity == "0";
				}
				var playerSupply:int = Mathf.Floor(player.resourceValue((resourceList[i] as MarketResource).resourceName()));
				if(playerSupply < input && playerSupply > 0){
					UIInputComponent.text = playerSupply.ToString();
					input = playerSupply;
				}
				totalPrice = market.calculateTotalSellingPrice(i, input).ToString();
				if(importPriceList[i] != importPrice || totalPrice != totalPriceList[i]){
					totalPriceList[i] = totalPrice;
					editLabel("TotalSellingPrice" + i, totalPrice);
				}
			}
		}
	}
	
	// Puts the val string in the UILabel component of gameObject
	function editLabel(gameObject:String, val:String){
		var gameObj:GameObject = GameObject.Find(gameObject);
		gameObj.GetComponent(UILabel).text = val;
	}
}
