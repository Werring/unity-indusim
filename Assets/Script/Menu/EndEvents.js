#pragma strict

class EndEvents extends MonoBehaviour {
    private var gameover:GameObject;
    private var itext:GameObject;
    private var credits:GameObject;

	function Start () {
			UIEventListener.Get(GameObject.Find("QuitBtn")).onClick = quitGame;
			UIEventListener.Get(GameObject.Find("CreditBtn")).onClick = showCredits;
	        gameover = GameObject.Find("GameOver");
	        itext = GameObject.Find("InfoText");
	        credits = GameObject.Find("Credits");
	        credits.SetActive(false);
	}
	
	// The game is over.
	function quitGame(go:GameObject){
	    print("Quit!");
	    Application.Quit();
	}

	// Start credits
	function showCredits(go:GameObject){
	    print("Show credits!");
	    gameover.SetActive(!gameover.activeSelf);
	    itext.SetActive(!itext.activeSelf);
	    credits.SetActive(!credits.activeSelf);
	}
}