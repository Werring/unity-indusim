#pragma strict

class UIHostingMenu extends NetworkBehaviour {
	var hostStartBtn:GameObject;
	var hostStartGameBtn:GameObject;
	var hostStopBtn:GameObject;
	var backBtn:GameObject;

	var portInputBox:UILabel;
	var announceLabel:UILabel;
	var lobbyLabel:UILabel;

	var portSelector:GameObject;
	var playerPanel:GameObject;

	var color:String;

	// Host server
	function hostFunc(go:GameObject){
		var port:int;
		if(int.TryParse(portInputBox.text, port)){
			networking.InitializeServer(10, port);
			updatePlayerList();
			TogglePanels(true);
		}else{
			Debug.Log("Incorrecte poort nummer.");
		}
	}

	// Stop hosting
	function stopHostFunc(go:GameObject){
		networking.Disconnect();
		TogglePanels(false);
		lobbyLabel.text = "";
	}

	// Host game.
	function startGameFunc(go:GameObject){
        (GameObject.Find("_sceneScripts").GetComponent("GameStarter") as GameStarter).triggerStart();
	}

	// Start game.
	function Start() {
		super.Start();

		UIEventListener.Get(hostStartBtn).onClick = hostFunc;
		UIEventListener.Get(hostStartGameBtn).onClick = startGameFunc;
		UIEventListener.Get(hostStopBtn).onClick = stopHostFunc;
		UIEventListener.Get(backBtn).onClick = stopHostFunc;
        registerRPC("startGame",startGame);
	}

	// Refresh connected players list and IP
	function Update() {
		if(announceLabel != null && Network.peerType == NetworkPeerType.Server) {
			announceLabel.text = "Het spel is bereikbaar via\n[-]IP: [" + color + "]"
			+	Network.player.ipAddress;
			updatePlayerList();
		}
	}

	// Toggle lobby
	private function TogglePanels(lobby:boolean) {
		portSelector.SetActive(!lobby);
		playerPanel.SetActive(lobby);
	}

	// Update connected players list.
	private function updatePlayerList() {
		var playerList:Hashtable = GameObject.Find("NetworkScripts").GetComponent(Players).playerList;
		var playerText:String = "";
		for(var key:Object in playerList.Keys) {
			var playstring:String = playerList[key] as String;
			playerText += playstring + "\n";
		}
		lobbyLabel.text = playerText;
	}

	// Switch to game scene
	function startGame() {
//		GameObject.Find("JoinMenu").GetComponent(UIJoinMenu).startGame();
		Application.LoadLevel(1);
	}
}
