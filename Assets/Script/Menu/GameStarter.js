#pragma strict

class GameStarter extends NetworkBehaviour {
	// Remote procedure, register start.
    function Start(){
        super.Start();
        registerRPC("startGame",startGame);
    }

	// Start games if is host.
    function triggerStart(){
        if(networking.isMine()){
            sendRPC("startGame",RPCMode.All);
        }
    }

    function startGame(){
        Application.LoadLevel(1);
    }
}