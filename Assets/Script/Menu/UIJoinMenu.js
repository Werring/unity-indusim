#pragma strict

class UIJoinMenu extends NetworkBehaviour {
	var joinBtn : GameObject;
	var exitBtn : GameObject;
	
	var joinPanel : GameObject;
	var lobbyPanel : GameObject;
	
	var ipInputBox : UILabel;
	var portInputBox : UILabel;
	
	var lobbyLabel : UILabel;

	// Join host.
	function joinFunc(go : GameObject) {
		var i : int;
		if(int.TryParse(portInputBox.text, i)) {
			networking.Connect(ipInputBox.text, i);
		}else{
			Debug.Log("Incorrecte poort nummer.");
		}
	}

	// Leave host
	function leaveFunc(go : GameObject) {
		networking.Disconnect();
		TogglePanels(false);
	}

	// Start / join
	function Start() {
		super.Start();
		UIEventListener.Get(joinBtn).onClick = joinFunc;
		UIEventListener.Get(exitBtn).onClick = leaveFunc;
	}

	// Load "Game" scene
	function startGame() {
		Application.LoadLevel(1);
	}

	// Refresh connected players list and IP
	function updatePlayerList() {
		var players:Players = GameObject.Find("NetworkScripts").GetComponent(Players);
		var playerList:Hashtable = players.playerList;
		var playerText:String = "";
		for(var key:Object in playerList.Keys) {
			var playstring:String = playerList[key] as String;
			/*if(playerText == ""){
				playerText += "" + playstring + " [FF6600](Host)[-]\n";
			}else*/
			if(playstring == players.me.name){
				playerText += "[00FF00]" + playstring + "[-]\n";
			}else{
				playerText += playstring + "\n";
			}
		}
		lobbyLabel.text = playerText;
	}

	// Toggle lobby pannel on connect
	function OnConnect() {
		TogglePanels(true);
		updatePlayerList();
	}

	// Update player list.
	function Update() {
		updatePlayerList();
	}

	// Reconnect.
    function OnReconnect(){
        OnConnect();
    }

	// Toggle lobby pannel
	private function TogglePanels(lobby : boolean) {
		joinPanel.SetActive(!lobby);
		lobbyPanel.SetActive(lobby);
	}
}