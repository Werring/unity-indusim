#pragma strict

class PlayerStat extends MonoBehaviour {
	private var userid : int;
	private var playername : String;

	// Set user ID;
	function setUserId(i: int) {
		this.userid = i;
	}

	// get user ID
	function getUserId():int{
		return this.userid;
	}

	// Set player name
	function setPlayerName(nm : String) {
		this.playername = nm;
	}

	// Return player name
	function getPlayerName():String{
		return this.playername;
	}
}