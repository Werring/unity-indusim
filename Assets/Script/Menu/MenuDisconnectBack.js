#pragma strict

class MenuDisconnectBack extends MonoBehaviour {
	private var networking : Networking;

	var myFunc = function(go : GameObject) {
		networking.Disconnect();
	};
	
	function Start() {
		networking = GameObject.Find("NetworkScripts").GetComponent(Networking);
		UIEventListener.Get(this.gameObject).onClick = myFunc;
	}
}