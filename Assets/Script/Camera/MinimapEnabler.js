#pragma strict

class MinimapEnabler extends MonoBehaviour {
	var minimapBtn : GameObject;
	var minimapCam : Camera;
	
	var state : boolean = true;

	// Init minimap
	function Start() {
		UIEventListener.Get(minimapBtn).onClick = minimapCollapsing;
	}

	// Set minimap state
	function minimapCollapsing(go : GameObject) {
		if(minimapCam.gameObject.activeSelf) {
			state = false;
			minimapCam.gameObject.SetActive(false);
		} else {
			state = true;
		}
	}

	// Expanding done.
	function DoneExpanding() {
		if(state) {
			minimapCam.gameObject.SetActive(true);
		}
	}
}