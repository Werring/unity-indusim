#pragma strict

class EyeMovement extends MonoBehaviour {

	var cam : Camera;

	// Move, zoom and rotate camera depending on keys pressed.
    function Update () {
        moveCamera();
       
        ////////////////////
        //zooming
        var Eye : GameObject = GameObject.Find("Eye");

        if (Input.GetAxis("Mouse ScrollWheel") > 0 && Eye.camera.orthographicSize > 20){
            Eye.camera.orthographicSize = Eye.camera.orthographicSize - 4;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && Eye.camera.orthographicSize < 80){
            Eye.camera.orthographicSize = Eye.camera.orthographicSize + 4;
        }
     
        //default zoom
        if (Input.GetKeyDown(KeyCode.Mouse2)){
            Eye.camera.orthographicSize = 50;
        }
           
        //////////////////
        //rotation
        if(Input.GetKey ("q")){
            transform.Translate(2.5, 0, -2.5);
        	transform.Rotate(0, -1, 0);
        } else if(Input.GetKey ("e")){
            transform.Translate(-2.5, 0, 2.5);
        	transform.Rotate(0, 1, 0);
        }         
    }

    // Move camera.
    private function moveCamera(){
    	//raycast and if terrain not hit move
    	//multiple rays to decide which direction to move the camera.
    	//casting one ray and undo last move action doesn't work with rotation
		var distance = 300;
		var up = true;
		var down = true;
		var left = true;
		var right = true;
		var ray : Ray = cam.ScreenPointToRay(new Vector2(Screen.width/2-distance, Screen.height/2));//left
		var hit : RaycastHit;
		if (!Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
			left = false;
		}
		ray = cam.ScreenPointToRay(new Vector2(Screen.width/2+distance, Screen.height/2));//right
		if (!Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
			right = false;
		}
		ray = cam.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2-distance));//down
		if (!Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
			down = false;
		}
		ray = cam.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2+distance));//up
		if (!Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
			up = false;
		}
		
		/////////////////////
        //keyboard scrolling
        var translationX : float = Input.GetAxis("Horizontal");
        var translationY : float = Input.GetAxis("Vertical");
	
	    ////////////////////
        //mouse scrolling
       
        var mousePosX = Input.mousePosition.x;
        var mousePosY = Input.mousePosition.y;
        var scrollDistance : int = 5;
        var scrollSpeed : float = 70;
	
		//Horizontal camera movement
        if (mousePosX < scrollDistance){					//horizontal, left
            translationX = -1.0;
        }
        if (mousePosX >= Screen.width - scrollDistance){	//horizontal, right
            translationX = 1.0;
        }
            
        //Vertical camera movement
        if (mousePosY < scrollDistance){					//scrolling down
            translationY = -1.0;
        }
        if (mousePosY >= Screen.height - scrollDistance){	//scrolling up
            translationY = 1.0;
        }
	        
	    if(!up && translationY > 0){
	    	translationY = 0;
	    }else if(!down && translationY < 0){
	    	translationY = 0;
	    }
	    
	    if(!right && translationX > 0){
	    	translationX = 0;
	    }else if(!left && translationX < 0){
	    	translationX = 0;
	    }
	    
	    if (Input.GetKey(KeyCode.LeftShift)){
			transform.Translate(translationX*2 + translationY*2, 0, translationY*2 - translationX*2);
		}else{
			transform.Translate(translationX + translationY, 0, translationY - translationX);
		}
    }
}