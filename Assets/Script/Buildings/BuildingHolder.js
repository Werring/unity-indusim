#pragma strict

// Initialize building holder.
function Awake () {
    if(GameObject.Find("/_globalScripts")){
        var globalBuildingScripts:GameObject = GameObject.Find("/_globalScripts/BuildingHolder");
        if(globalBuildingScripts == null){
            this.gameObject.transform.parent = GameObject.Find("/_globalScripts").transform;
            DontDestroyOnLoad(this.gameObject);
        } else {
            Destroy(this.gameObject);
        }
    }
}
