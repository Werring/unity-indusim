#pragma strict

class Building extends MonoBehaviour{

	private var registry:Registry;
	private static var buildingPlacer:BuildingPlacer;
	var template:BuildingTemplate;
	var position : Vector3;
	var upgrades = new Array();
	var buildingType:String = "";
	var resourceMultiplier:float = 1.0;
	var externalInfluenceMultiplier:float = 1.0;

	var renderers:Array;

	public static var productionTime = 10; // seconds to produce 1 count.

    private var _x:int;
    private var _z:int;

    private var economyStat:StatsStorage;

    // Initialize a building.
	function init(x:int, z:int){
	    economyStat = StatsStorage.getStat("Totale Inkomsten");
	    registry = GameObject.Find("_sceneScripts").GetComponent(Registry);
        buildingType = this.name.ToString().Replace("(Clone)", "");
		buildingPlacer = GameObject.Find("_sceneScripts").GetComponent(BuildingPlacer);

        while(template == null || template.produce == null || template.require == null){
            yield WaitForSeconds(0.1);
            template = registry.getBuildingTemplate(buildingType);
        }

		position.x = Mathf.Floor(x/10);
		position.z = Mathf.Floor(z/10);

	    registry.addBuilding(this);
		renderers = this.GetComponents(Renderer);


	    if(template.produce.Count + template.require.Count > 0){
			InvokeRepeating("resourceUpdate", productionTime, productionTime);
		}
	}

	/* Upgrade management, not used.
	function getUpgrades():AppliedTechnology{
		return upgrades as AppliedTechnology;
	}

	function addUpgrade(appliedTechnology:AppliedTechnology){
		upgrades.push(appliedTechnology);
	}
	*/

	// Get a hastable that describes what this building produces and requires, format: {"product a": value(float),
	//																					"product b": value(float)}
	function getProductions():Hashtable{
		var productions:Hashtable = new Hashtable();
		for(var i:int = 0; i < template.require.Count; i++){
			var product:Array = template.require[i] as Array;
			var resource:double = registry.player.resourceValue(product[0].ToString());
			var require:double = double.Parse(product[1].ToString()) * resourceMultiplier * externalInfluenceMultiplier;
			productions.Add(product, -require);
		}
		for(i = 0; i < template.produce.Count; i++){
			product = template.produce[i] as Array;
			resource = registry.player.resourceValue(product[0].ToString());
			var produce:double = double.Parse(product[1].ToString()) * resourceMultiplier * externalInfluenceMultiplier;
			productions.Add(product, produce);
		}
		return productions;
	}

	// Produce the building's products, if enough supplies are available.
	function resourceUpdate(){
		//check if there are enough resources
		var canProduce:boolean = true;
		for(var i:int = 0; i < template.require.Count; i++){
			var product:Array = template.require[i] as Array;
			var resource:double = registry.player.resourceValue(product[0].ToString());
			var require:double = double.Parse(product[1].ToString()) * resourceMultiplier * externalInfluenceMultiplier;
			if(resource < require){
				canProduce = false;
			}
		}

		if(canProduce){
			//remove required resources
			for(i = 0; i < template.require.Count; i++){
				product = template.require[i] as Array;
				resource = registry.player.resourceValue(product[0].ToString());
				require = double.Parse(product[1].ToString()) * resourceMultiplier * externalInfluenceMultiplier;
				registry.player.setResourceValue(product[0].ToString(),resource - require);
			}

			//add produced resources
			for(i = 0; i < template.produce.Count; i++){
				product = template.produce[i] as Array;
				resource = registry.player.resourceValue(product[0].ToString());
				var incrementingResource:double = double.Parse(product[1].ToString()) * resourceMultiplier * externalInfluenceMultiplier;
				resource += incrementingResource;
				registry.player.setResourceValue(product[0].ToString(),resource);
				economyStat.addData(product[0].ToString(),incrementingResource,true);
			}
		}
	}

	// Click handler for the building to display resource production.
	function onClicked(){
		buildingPlacer.setClicked(this);

		var button : GameObject;
		button = GameObject.Find("deleteButton");
		UIEventListener.Get(button).onClick = removeBuilding;

		var label : UILabel;
		label = GameObject.Find("buildingInformationLabel").GetComponent("UILabel") as UILabel;
		var text = template.displayName + "\n";
		text += "[bed8fe]Kosten:[-]";
		for(var i = 0; i < template.cost.length; i++){
			var resourceCost : Resource = new Resource(template.cost[i]);
			text += "\n  " + resourceCost.resourceName + ": " + resourceCost.amount;
		}
		if(i == 0){
		    text += "\n  -";
		}
		text += "\n[00DD00]Maakt:[-]";
		for(i = 0; i < template.produce.length; i++){
			var resourceProduce : Resource = new Resource(template.produce[i]);
			var temp : double = resourceProduce.amount * resourceMultiplier * externalInfluenceMultiplier;
			text += "\n  " + resourceProduce.resourceName + ": " + Util.mathRound(temp, 2);
		}
		if(i == 0){
		    text += "\n  -";
		}
		text += "\n[DD0000]Gebruikt:[-]";
		for(i = 0; i < template.require.length; i++){
			var resourceRequired : Resource = new Resource(template.require[i]);
			temp = resourceRequired.amount * resourceMultiplier * externalInfluenceMultiplier;
			text += "\n  " + resourceRequired.resourceName + ": " + Util.mathRound(temp, 2);
		}
		if(i == 0){
		    text += "\n  -";
		}
		label.text = text;
	}

	// Remove a building.
	function removeBuilding() {
		var influencedBuildings:GameObject[] = Util.FindGameObjectsFromTags(template.influences);
		for (var go:GameObject in influencedBuildings){
			var building:Building = go.GetComponent(Building) as Building;
			if(	building != null && (this.gameObject.transform.position - building.transform.position).sqrMagnitude < 20000){
				building.externalInfluenceMultiplier /= 0.9;
	        }
	    }

		Debug.Log("Building removed");

		//set the building information invisible
		var button : GameObject;
		button = GameObject.Find("deleteButton");
		UIEventListener.Get(button).onClick = null;

		var label : UILabel;
		label = GameObject.Find("buildingInformationLabel").GetComponent("UILabel") as UILabel;
		label.text = "";

		//return half of the build cost
		for(var i = 0; i < template.cost.length; i++){
			var product:Array = template.cost[i] as Array;
			var playerResource:int = registry.player.resourceValue(product[0].ToString());
			var addResource:double = double.Parse(product[1].ToString())/2;
			registry.player.setResourceValue(product[0].ToString(),playerResource + addResource);
		}
		if(buildingPlacer == null){
			buildingPlacer = GameObject.Find("_sceneScripts").GetComponent(BuildingPlacer);
		}
		buildingPlacer.setProductions();
		Destroy(this.gameObject);
	}

	// Change the material for a building, to indicate it being selected or not.
	function setMaterial(mat:Material){
		if(renderers != null && renderers[0] != null){
			(renderers[0] as Renderer).material = mat;
		}
	}
}
