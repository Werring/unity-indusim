#pragma strict

class BuildingPlacer extends MonoBehaviour {

	private var dragState : boolean = false;
	private var rotated : boolean = false;
	private var dragObject : Transform;
	private var gridWidth:int = 500;
	private var gridHeight:int = 500;
	private var grid:Light;
	private var resourceValue:int = -1;

	static private var materialsDefault:Hashtable;
	static private var materialsHighlight:Hashtable;

	enum Place {Bakery, Brewery, BrickFactory, Church, Claypit, ClothFactory, Farm, House, Ironmine, Ranch, Road, Smith, Spinner, SugarFactory, Warehouse}

	var place : Place;

	var uicam : UICamera;
	var enoughResources = true;
	var allowPlacement : boolean;

	var registry: Registry;
	var resourceMapper: ResourceMapper;
	static var clickedBuilding:Building;
	static var clickedDisplayName:String;

	var cam : Camera;
	var layerMask : LayerMask;

	var gridX : float = 10;
	var gridY : float;
	var gridZ : float = 10;

	var template : BuildingTemplate;

    var xPosRes : int = 20;
    var yPosRes : int = 200;

    var xBuildOffset : int = 10;

	// Initialize some variables.
	function Start() {
		uicam.fallThrough = this.gameObject;

		var go : GameObject;
		go					= GameObject.Find("_sceneScripts");
	    registry			= go.GetComponent(Registry);
	    resourceMapper		= go.GetComponent(ResourceMapper);
	    grid				= GameObject.Find("Grid").light;
	    gridWidth			= resourceMapper.terrain.terrainData.size.x;
	    gridHeight			= resourceMapper.terrain.terrainData.size.z;
		materialsDefault	= new Hashtable();
		materialsHighlight	= new Hashtable();
	}

	// Click handler, build a building if enough resources are available, and the building is positioned in a spot that it can be built at. Recalculate productions after placement.
	function OnClick() {
		var ray : Ray = cam.ScreenPointToRay(Input.mousePosition);
		var hit : RaycastHit;
		var resourceCost : Resource;

		if (Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, layerMask) &&
			dragState &&
			!dragObject.GetComponent(BuildingCollision).obstructedArea &&
			allowPlacement) {

			if(enoughResources || Util.DEBUG){
				for(var i = 0; i < template.cost.length; i++){
					resourceCost = new Resource(template.cost[i]);
					registry.player.setResourceValue(resourceCost.resourceName,registry.player.resourceValue(resourceCost.resourceName)-resourceCost.amount);
				}
				var building : Transform;
                building = Instantiate(template.prefab, SnapGrid(hit.point), dragObject.rotation);
                building.parent = (GameObject.Find("BuildingHolder") as GameObject).transform;
				var built:Building = building.GetComponent(Building) as Building;

                built.resourceMultiplier = (resourceValue != 0)? 1.0 : 0.5;
                built.buildingType = template.buildingTemplateName;
                built.template = registry.getBuildingTemplate(built.buildingType);

				setMultipliers(template.influences, template.influencedBy, building);

				if(!materialsDefault.ContainsKey(template.displayName)){
					var defaultMaterial:Material = (built.GetComponentsInChildren(Renderer)[0] as Renderer).material;
					materialsDefault.Add(template.displayName,defaultMaterial);
					if(!materialsHighlight.ContainsKey(template.displayName)){
						var highlightMaterial:Material = new Material(Shader.Find("Transparent/Diffuse"));
						highlightMaterial.mainTexture = defaultMaterial.mainTexture;
						highlightMaterial.color = new Color(0,1,0,0.5);
						materialsHighlight.Add(template.displayName,highlightMaterial);
					}
				}
				setProductions();
			}
			//place multiple with shift
			if(Input.GetKey(KeyCode.LeftShift) == false){
				UnbindBuilding();
			} else {
				BindBuilding();
			}
		}
	}

	/* Move the building dragger to the mouse position,
		change the dragger's colourse depending on:
			collisions,
			resources available,
			resource values in the ground.
		Rotate the building if [Z]/[X] are pressed,
		Delete the building if [Delete] is pressed,
		Manage building selection on click.
	*/
	function Update () {
		var ray : Ray = cam.ScreenPointToRay(Input.mousePosition);
		var hit : RaycastHit;

		if(template != null && template.cost != null){
			enoughResources = true;
			for(var i = 0; i < template.cost.length; i++){
				var resourceCost : Resource = new Resource(template.cost[i]);
				var currentResource : int = registry.player.resourceValue(resourceCost.resourceName);
				if(currentResource < resourceCost.amount)
					enoughResources = false;
			}
		}
		if(dragState && dragObject != null) {
			if(Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, layerMask)) {

				var hx:int = template.size.x * 5;
				var hz:int = template.size.z * 5;

				if(rotated){
					var temp:int = hx;
					hx = hz;
					hz = temp;
				}
				var buildingCollision:BuildingCollision = dragObject.GetComponent(BuildingCollision);
				var snappedPos:Vector3 = SnapGrid(hit.point);
				if(snappedPos.x >= hx && snappedPos.x <= gridWidth-hx &&
				   snappedPos.z >= hz && snappedPos.z <= gridHeight-hz){
					allowPlacement = true;
					if(buildingCollision.obstructedArea == false){
						buildingCollision.setOriginalMaterial();
					}
				}else{
				 	buildingCollision.setErrorMaterial();
				 	allowPlacement = false;
				}
				if(!enoughResources && !Util.DEBUG){
					buildingCollision.setErrorMaterial();
					allowPlacement = false;
				}

				dragObject.position = snappedPos;

				if(template != null){
					var gos:GameObject[] = GameObject.FindGameObjectsWithTag("Building");
					if(template.resource != null && template.resource != ""){
						var resourceZ = (snappedPos.z / 500) * 512;
						var resourceX = (snappedPos.x / 500) * 512;
						resourceValue = resourceMapper.perlin(template.resource, resourceZ, resourceX);
						if(!buildingCollision.getErrorMaterialSet() && resourceValue == 0){
							buildingCollision.setLowProductionMaterial();
						}
					}else{
						resourceValue = -1;
					}
				}
			}
		} else {
		    allowPlacement = false;
		}

		// Cancel drag on escape or right mouse button
		if(dragState && (Input.GetKeyUp(KeyCode.Escape) || Input.GetMouseButtonUp(1))){
			UnbindBuilding();
			rotated = false;
		}

		//rotate with z and x
		if(dragState && Input.GetKeyUp(KeyCode.Z)){
			dragObject.Rotate(0, 0, 90);
			rotated = !rotated;
		}else if(dragState && Input.GetKeyUp(KeyCode.X)){
			dragObject.Rotate(0, 0, -90);
			rotated = !rotated;
		}

		if(Input.GetKeyUp(KeyCode.Delete) && clickedBuilding != null){
			clickedBuilding.removeBuilding();
			clickedBuilding = null;
		}

		//raycast and if building hit call onclicked
		if(Input.GetMouseButtonDown(0)){//(right mouse button) left mouse button would call onclick when building is placed
			if(Physics.Raycast(ray.origin, ray.direction, hit, Mathf.Infinity, (1 << LayerMask.NameToLayer("Buildings")) | (1 << LayerMask.NameToLayer("Ground")))) {
				var building : Building = hit.collider.gameObject.GetComponent(typeof(Building));
				if(building != null){
					building.onClicked();
				}else{
					setClicked(null);
				}
			}
		}
	}

	// Initialize a building to build.
	function BindBuilding():boolean {
		var rotation:Quaternion;
		var saveRotation:boolean = false;
		if(dragState){
			var r:Quaternion = dragObject.transform.rotation;
			if(r.x != 0 && r.y != 0 && r.z != 0){
				saveRotation = true;
				rotation = r;
			}
			UnbindBuilding();
		}
		template = registry.getBuildingTemplate(place.ToString()) as BuildingTemplate;
		dragObject = Instantiate(template.dragger, Vector3(0,99999999999,0), template.dragger.rotation);
		if(saveRotation){
			dragObject.transform.rotation = rotation;
		}else{
			rotated = false;
		}

		resourceMapper.showResource(template.resource);
		grid.enabled = true;
		dragState = true;
		return true;
	}

	// Reset dragger.
	function UnbindBuilding():boolean {
		dragObject.position = Vector3(99999999999999, 99999999999999, 99999999999999);
		Destroy(dragObject.gameObject);
		resourceMapper.hideResources();
		grid.enabled = false;
		dragState = false;
		return true;
	}

	// Snap a vector to grid points.
	function SnapGrid(vec:Vector3):Vector3 {
		//tel de positie van de pefab dragger er bij op om het te fine tunen
		var offsetX:int = (template.size.x % 2 == 1) ? 5 : 0;
		var offsetY:int = (template.size.z % 2 == 1) ? 5 : 0;
		var pos : Vector3;
		pos.x = (Mathf.Round((vec.x-offsetX) / gridX) * gridX + template.dragger.transform.position.x);
		pos.y = vec.y + template.dragger.transform.position.y;
		//pos.y = Mathf.Round(vec.y / gridY) * gridY;
		pos.z = (Mathf.Round((vec.z-offsetY) / gridZ) * gridZ + template.dragger.transform.position.z);

        return pos;
	}

	// Set buildings' productions within a range of the placed building.
	function setMultipliers(influences:Array, influencedBy:Array, placedBuilding:Transform){
		var influencedBuildings:GameObject[] = Util.FindGameObjectsFromTags(influences);
		var influenceBuildings:GameObject[] = Util.FindGameObjectsFromTags(influencedBy);

		for (var go:GameObject in influencedBuildings){
			var building:Building = go.GetComponent(Building) as Building;
			if(	building != null && (placedBuilding.position - building.transform.position).sqrMagnitude < 20000){
				building.externalInfluenceMultiplier *= 0.9;
	        }
	    }
		for (var go:GameObject in influenceBuildings){
			building = go.GetComponent(Building) as Building;
			if(	building != null && (placedBuilding.position - building.transform.position).sqrMagnitude < 20000){
				(placedBuilding.GetComponent(Building) as Building).externalInfluenceMultiplier *= 0.9;
	        }
	    }
	}

	// Set a the productions in the registry to display in the GUI.
	function setProductions(){
   		yield WaitForSeconds(0.1);
	    var tempProductions:Hashtable = new Hashtable();

	    //setProduction(built, tempProductions);
	    var allBuildings:Building[] = GameObject.FindObjectsOfType(Building);
	    for (building in allBuildings){
			getProduction(building, tempProductions);
	    }

	    registry.productions = tempProductions;
	}

	// Get the buildings productions, and save it to the supplied hashtable.
	function getProduction(building:Building, productionsList:Hashtable){
        if(building.buildingType != ""){
        	var productions:Hashtable = building.getProductions();
        	for(var key:Object in productions.Keys){
        		if(productionsList.ContainsKey((key as Array)[0])){
        			var val:float = float.Parse(productionsList[(key as Array)[0]].ToString());
					productionsList[(key as Array)[0]] = val + float.Parse(productions[key].ToString());
        		}else{
        			productionsList.Add((key as Array)[0].ToString(), float.Parse(productions[key].ToString()));
        		}
			}
        }
	}

	// Manage what building is currently selected.
	function setClicked(building:Building){
        if(building != null && building.template != null && !materialsDefault.ContainsKey(building.template.displayName)){
            var defaultMaterial:Material = (building.GetComponentsInChildren(Renderer)[0] as Renderer).material;
            materialsDefault.Add(building.template.displayName,defaultMaterial);
            if(!materialsHighlight.ContainsKey(building.template.displayName)){
                var highlightMaterial:Material = new Material(Shader.Find("Transparent/Diffuse"));
                highlightMaterial.mainTexture = defaultMaterial.mainTexture;
                highlightMaterial.color = new Color(0,1,0,0.5);
                materialsHighlight.Add(building.template.displayName,highlightMaterial);
            }
        }
		if(clickedBuilding != null){
			clickedBuilding.setMaterial(materialsDefault[clickedDisplayName] as Material);
		}
		if(building != null){
			building.setMaterial(materialsHighlight[building.template.displayName] as Material);
			clickedDisplayName = building.template.displayName;
		}
		clickedBuilding = building;
	}
}
