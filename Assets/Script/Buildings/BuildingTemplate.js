#pragma strict

class BuildingTemplate {
	var buildingTemplateName:String;
	var displayName:String;
	var description:String;
	var size:Vector3;
	var prefab:Transform;
	var dragger:Transform;
	var cost:Array;
	var produce:Array;		// What the building produces
	var require:Array;		// What it requires to produce this
	var resource:String;	// What resource it's production is based on (in the terrain);
	var influencedBy:Array;	// What buildings negatively impact this building's production.
	var influences:Array;	// What buildings negatively impact this building's production.
	
	// Initialize a building template.
	function BuildingTemplate(	name		:String,
								displayName	:String,
								description	:String,
								width		:int,
								height		:int,
								prefab		:GameObject,
								dragger		:GameObject,
								cost		:Array,
								produce		:Array,
								require		:Array,
								resource	:String,
								influencedBy:Array,
								influences	:Array){
								
		this.buildingTemplateName	= name;
		this.displayName			= displayName;
		this.description			= description;
		this.size					= new Vector3(width, -1 , height);
		this.prefab					= prefab.transform;
		this.dragger				= dragger.transform;
		this.cost					= cost;
		this.produce				= produce;
		this.require				= require;
		this.resource				= resource;
		this.influencedBy			= influencedBy;
		this.influences				= influences;
	}
}