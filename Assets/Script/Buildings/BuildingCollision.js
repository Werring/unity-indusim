 #pragma strict
import System.Collections.Generic;

class BuildingCollision extends MonoBehaviour{
	private var layerMask : int = LayerMask.NameToLayer("Buildings");

	private var originalMaterial:Material;
	var errorMaterial:Material;
	var lowProductionMaterial:Material;
	private var errorMaterialSet:boolean = false;
	private var _obstructedArea: boolean = false;

	private var collisionList :List.<int> = new List.<int>();

	function Start(){
		originalMaterial = this.renderer.material;
	}
	
	// Check if the building / dragger collides with another.
	function Update(){
		if(collisionList.Count > 0) {
			setErrorMaterial();
		}
	}

	// Handle a collision start event
	function OnCollisionEnter(hit: Collision) {
		if(hit.gameObject.layer == layerMask) {
			setErrorMaterial();
			_obstructedArea = true;
			collisionList.Add(hit.gameObject.GetInstanceID());
		}
	}

	// Set the dragger's material to the error material.
	public function setErrorMaterial(){
		this.renderer.material = errorMaterial;
		errorMaterialSet = true;
	}

	// Set the dragger's material to the "Low production" material.
	public function setLowProductionMaterial(){
		this.renderer.material = lowProductionMaterial;
		errorMaterialSet = false;
	}

	// Reset the dragger's material.
	public function setOriginalMaterial(){
		if(originalMaterial){
			this.renderer.material = originalMaterial;
		}
		errorMaterialSet = false;
	}
	
	// return if the error material is set
	function getErrorMaterialSet():boolean{
		return errorMaterialSet;
	}

	// Handle a collision exit event
	function OnCollisionExit(hit : Collision) {
		if(hit.gameObject.layer == layerMask) {
			collisionList.Remove(hit.gameObject.GetInstanceID());
			setOriginalMaterial();
			if(collisionList.Count == 0) {
				_obstructedArea = false;
			}
		}
	}
	
	// Get obstructed area.
	public function get obstructedArea():boolean{
	    return _obstructedArea;
	}
}
