public class Util {
	static function mathRound(val:double){
		return Mathf.Floor(val+0.5);
	}

	static function getTime(){
		var epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		return timestamp;
	}

	static function resourcesToJSON(resources:Array){
    	var json:String = "[";
    	for(var i:int = 0; i < resources.length; i++){
    		var res:MarketResource = resources[i];
    		var name:String = res.resourceName();
    		var startPrice:int = res.startPrice();
    		var demand:int = res.demand();
    		var supply:double = res.supply();
    		var demandConstant:double = res.demandConstant();
    		var eqilibrium:int = res.eqil();
    		var population:double = Population.size();
    		var resourceJson = "{\"startPrice\":\"" + startPrice + "\"";
    		resourceJson += ",\"name\":\"" + name + "\"";
    		resourceJson += ",\"demand\":\"" + demand + "\"";
    		resourceJson += ",\"supply\":\"" + supply + "\"";
    		resourceJson += ",\"eqil\":\" " + eqilibrium + "\"";
    		resourceJson += ",\"demandConstant\":\"" + demandConstant + "\"}";
    		resourceJson += ",";

    		json += resourceJson;
    	}
    	json += "{\"Population\":" + population + "}]";
    	Debug.Log(json);
    	return json;
    }

    static function JSONToResources(json:String){
    	var mainJSON:JSONObject = new JSONObject(json);
    	var resourceArray = new Array();
    	var population:double;
    	for(var i:int = 0; i < mainJSON.list.Count; i++){
    		var subJSON:JSONObject = mainJSON.list[i];
    		if(subJSON.list.Count > 1){
    			var marketRes:MarketResource = JSONObjectToMarketResource(subJSON);
    			resourceArray.Push(marketRes);
    		}else{
    			var JSONPopulationObject:JSONObject = subJSON.list[0];
    			var JSONPopulationNumber:JSONObject = JSONPopulationObject;
    			population = JSONPopulationNumber.n;
    		}
    	}
    	for(var h:int = 0; h < resourceArray.length; h++){
    		var res:MarketResource = resourceArray[h];
    		res.calculateDemand(population);
    	}
    	return resourceArray;
    }

	private static function JSONObjectToMarketResource(json:JSONObject){

		var name:String = "";
		var demand:int = 0;
		var supply:double = 0;
		var eqil:int = 0;
		var startPrice:int = 0;
		var demandConstant:double = 0;

		for(var k:int = 0; k < json.list.Count; k++){
			var key:String = json.keys[k];
			var j:JSONObject = json.list[k];
			switch(key){
				case "name":
					name = j.str;
					break;
				case "demand":
					demand = int.Parse(j.str);
					break;
				case "supply":
					supply = parseFloat(j.str);
					break;
				case "eqil":
					eqil = int.Parse(j.str);
					break;
				case "startPrice":
					startPrice = int.Parse(j.str);
					break;
				case "demandConstant":
					demandConstant = parseFloat(j.str);
					break;
			}

		}

		var res:MarketResource = new MarketResource(name, startPrice, eqil, supply, demandConstant);
		return res;
	}
}

