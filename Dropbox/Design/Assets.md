#Currency
 - Florijnen

##Ruw Grondstoffen
 - Staal
 - Katoen
 - Suikerbiet
 - Granen
 - Vlees
 - Klei 
 
##Export producten
 - Textiel
 - Voedsel
 	- Brood
 	- Suiker
 	- Bier 	
 - Kleding
 - Tools
 - Baksteen
 

##Resource tree
	+ Smelterij +-> Metaal +-> Tools      +-> Upgrade fabriek
	|                      \-> Fabrieken
	|                     
	+ Boerderij +-> Vee    +-> Vlees
	|           |          
	|           \-> Akkers +-> Granen     +-> Brouwerij     +-> Bier
	|                      |              \-> Bakkerij      +-> Brood
	|                      |
	|                      \-> Suikerbiet +-> Suikerfabriek +-> Suiker
	|	
	+ Kleiput   +-> Klei   +-> Kleioven   +-> baksteen      +-> Fabrieken
	|                                                       +-> Huizen         +-> Arbeiders
	|                                                       \-> Wegen upgrade
	|
	+ Stadspoort +->Katoen +-> Spinnerij  +-> Textiel       +-> Naaierij       +-> Kleding 
	|
	+ Magazijn
	|
	+ Hoofdgebouw
	

##Gebouwen
 - Productie
 	- Smelterij
 	- Smederij
 	- Veeboerderij
 	- Akkerboederij
 	- Brouwerij
 	- Bakkerij
 	- Spinnerij
 	- Naaierij
 	- Suikerfabriek
 	- Kleioven
 	- Kleiput
 - Utiliteiten
 	- Arbeidershuisjes
 	- Magazijnen
 	- Stadspoort
 	- Hoofdgebouw
 - Wegen
 	- Zand
 	- Verhard
 	- Klinkers
 	
##Gebeurtenissen
 - 1733 Vliegspoel (Katoen++) 
 - 1764 Spinning Jenny
 - 1777 Mech weefgetouw (Kleding++)
 - 1785 Stoommachine spinnerij (Katoen++)
 - 1790 Stoommachine in ijzerwalserij (Metaal++)
 - 1793 Cotton Gin (Katoen++)
 - 1804 Automatiseerde weefmachine
 - 1810 Stoom locomotief (Markt Prijzen--)
 - 1820 Handweefgetouw vervangen
 - 1837 Stalen Ploeg (granen++, suikerbiet++)
 - 1839 Ziekte in sloppenwijken rapport (hint voor verbeteren plaatsing arbeiders, arbeiders++)
 - 1840 Telegraaf (IM mogelijk, berichten komen nu direct aan)

