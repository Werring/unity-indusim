Omvang======Binnen het project is het helaas niet mogelijk is om alle aspecten van de game tot in de puntjes uit te gaan werken.
Onze focus gaan we richten op de volgende punten - Gameplay
 - Historisch correct

Ons doel is om op een leuke mannier de geschiedenis en de werking en de oorzaak van de Industriële Revolutie.

Hierdoor zal onze aandacht minder gaan naar de onderdelen:
- Audio
- Modelleren

De bedoeling is om wel kleine audio effecten te gebruiken, maar het inspreken van teksten en het perfectioneren van geluidseffecten zal alleen gebeuren als hier nog voldoende tijd voor over is.

Het spel zal zich afspelen in de 19e eeuw en je werkt vanuit het oogpunt van een rijke zakenman die zijn fabriek moet gaan runnen.
Hierbij moet gelet worden op dingen als:
- geld en grondstoffen zoals:
	- voedsel
	- kolen
	- ijzer
	- hout
	- klei
	- etc.
- schaarste
- concurrentie
- ziekte
- espionage en sabotage
- woonruimte voor arbeiders