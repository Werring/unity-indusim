2013-04-02	-	Plan van Aanpak
2013-04-02	-	Game Design Document (h4)
2013-04-03	-	Onderzoek (AI & Pathfinding)
2013-04-04	-	Onderzoek (AI & Pathfinding)
2013-04-04	-	Onderzoek (Statistieken)
2013-04-04	-	Onderzoek (Game Logica)
2013-04-05	-	Klassendiagram
2013-04-08	-	Klassendiagram
2013-04-08	-	Bekend raken met Unity
2013-04-09	-	Grid display + snapping
2013-04-10	-	Grid display + snapping
2013-04-10	-	BuildingTarget
2013-04-11	-	Minimap maken
2013-04-12	-	Minimap maken
2013-04-15	-	UI
2013-04-16	-	UI
2013-04-18	-	UI
2013-04-19	-	nGUI - Onderzoek plugin, unity standaard ui te hoge drawcall
2013-04-22	-	nGUI - UI opnieuw maken
2013-04-23	-	nGUI - UI opnieuw maken
2013-04-24	-	nGUI - UI opnieuw maken
2013-04-25	-	nGUI - UI opnieuw maken
2013-04-29	-	nGUI - UI opnieuw maken
2013-05-06	-	nGUI - UI - ingame ui
2013-05-07	-	nGUI - UI - ingame ui
2013-05-08	-	nGUI - UI - ingame ui 
2013-05-09	-	nGUI - UI - ingame ui
2013-05-13	-	nGUI - UI - ingame ui
2013-05-14	-	nGUI - UI - ingame ui
2013-05-15	-	nGUI - UI - ingame ui
2013-05-16	-	nGUI - UI - Start schermen & networking
2013-05-20	-	nGUI - UI - Start schermen & networking
2013-05-21	-	nGUI - UI - Start schermen
2013-05-22	-	nGUI - UI - Start schermen
2013-05-23	-	nGUI - UI - Start schermen
2013-05-24	-	Gebruikerstest op UniC
2013-05-27	-	nGUI - Startscherm bijgewerkt
2013-05-28	-	nGUI - Game UI bijgewerkt & font toegepast
2013-05-29	-	Kleine UI bugs verholpen
2013-05-30	-	Tijdbalk synchroniseerd over het netwerk
2013-06-03	-	Tooltip aan bouwknoppen toegevoegd
2013-06-04	-	Hit detectie probleem opgelost
2013-06-05	-	Knop iconen bijgewerkt
2013-06-17	-	Iconen toegevoegt
2013-06-18	-	Bart geholpen met market UI
2013-06-19	-	Kleine gameplay bugs opgelost
2013-06-20	-	Goederen rechts bovenin de UI geplaatst incl. tooltip
2013-06-21	-	Productie aan tooltip goederen toegevoegd
2013-06-24	-	Klassendiagram gemaakt
2013-06-25	-	Klassendiagram gemaakt
2013-06-26	-	Eindverslag bijgewerkt
2013-06-27	-	Eindverslag afgemaakt