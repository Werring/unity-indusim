#pragma strict
class Timeline extends NetworkBehaviour
{
    private var startTime : float;
    private var restSeconds : int;
    private var roundedRestSeconds : int;
    private var displaySeconds : int;
    private var displayMinutes : int;

    var countDownSeconds : int;

    var properties:Object = new Object();
    private var minuteLeftNotification : System.Boolean = false;

    function Start() {
        super.Start();
        registerStateSync();
        registerRPC("alertOneMinute",alertOneMinute);

    }

    function Awake() {
        if(networkView.isMine){
            startTime = Time.time;
        }
    }

    function OnGUI () {
        if(networkView.isMine || Network.peerType == NetworkPeerType.Disconnected){
            var guiTime = Time.time - startTime;

            restSeconds = countDownSeconds - (guiTime);

            if (restSeconds == 60) {
                if(minuteLeftNotification == false && networkView.isMine){
                    sendRPC("alertOneMinute",RPCMode.All);
                    minuteLeftNotification = true;
                }
            }
            if (restSeconds == 0) {
                print ("Time is Over");
            }

            roundedRestSeconds = Mathf.CeilToInt(restSeconds);
            displaySeconds = roundedRestSeconds % 60;
            displayMinutes = roundedRestSeconds / 60;



            } else {
            displaySeconds = roundedRestSeconds % 60;
            displayMinutes = roundedRestSeconds / 60;
        }
        display(displayMinutes,displaySeconds);
        if(minuteLeftNotification){
            var tekst:String = "One minute left!";
            GUI.Label (Rect (400, 75, 100, 30), tekst);
        }
    }

    function display(displayMinutes:int,displaySeconds:int){
        var tekst = String.Format ("{0:00}:{1:00}", displayMinutes, displaySeconds);
        GUI.Label (Rect (400, 25, 100, 30), tekst);

        var tekst2 = String.Format ("{0}", roundedRestSeconds);
        GUI.Label (Rect (400, 125, 100, 30), tekst2);
    }

    function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo)
    {
        var rrs:int;
        if (stream.isWriting) {
            rrs = roundedRestSeconds;
            stream.Serialize(rrs);
        } else {
            stream.Serialize(rrs);
            roundedRestSeconds = rrs;
        }
    }

    function alertOneMinute(){
        if(minuteLeftNotification) print("Already true");
        minuteLeftNotification = true;
    }
}
