#pragma strict
class Networking extends MonoBehaviour {

	var connectToIP : String = "GET_LAN_IP";
	var connectPort : int = 25001;
    var userID : int = -1;
    private var _uid = 0;

    private var networkClasses:Array;
    private var rpc_functions:Hashtable;


    private var date:int = 0;


	function Start() {

        networkClasses =  new Array();
        rpc_functions = new Hashtable();
	}

	function Update() {

	}


	function OnGUI() {
        if(connectToIP == "GET_LAN_IP"){
            connectToIP = Network.player.ipAddress;
        }

        if (Network.peerType == NetworkPeerType.Disconnected){
            GUILayout.Label("Connection status: Disconnected");

            connectToIP = GUILayout.TextField(connectToIP, GUILayout.MinWidth(100));
            connectPort = parseInt(GUILayout.TextField(connectPort.ToString()));

            GUILayout.BeginVertical();
            if (GUILayout.Button ("Connect as client"))
            {

                Network.Connect(connectToIP, connectPort);
            }

            if (GUILayout.Button ("Start Server"))
            {
                Network.InitializeServer(10, connectPort, false);
            }
            GUILayout.EndVertical();


        }
        else
        {
            if (Network.peerType == NetworkPeerType.Connecting){
                GUILayout.Label("Connection status: Connecting");
            }
            else if (Network.peerType == NetworkPeerType.Client)
            {
                GUILayout.Label("Connection status: Client!");
                GUILayout.Label("Ping to server: "+Network.GetAveragePing(  Network.connections[0] ) );
            }
            else if (Network.peerType == NetworkPeerType.Server)
            {

                GUILayout.Label("Connection status: Server!");
                GUILayout.Label("Connections: "+Network.connections.length);
                if(Network.connections.length>=1)
            {
                    GUILayout.Label("Ping to first player: "+Network.GetAveragePing(  Network.connections[0] ) );
                }
            }

            if (GUILayout.Button ("Disconnect"))
            {
                Network.Disconnect(200);
            }
        }

	}

    function OnServerInitialized()
    {
        Debug.Log("Server initialized and ready");
        if(userID == -1)
        {
            playerID(++_uid);
        }
    }

	function OnConnectedToServer() {
        Debug.Log("This CLIENT has connected to a server");
        if(userID == -1)
        {
            networkView.RPC("getPlayerID",RPCMode.Server);
        }
    }

    /*
    function OnDisconnectedFromServer(info : NetworkDisconnection) {
        Debug.Log("This SERVER OR CLIENT has disconnected from a server");
    }

    function OnFailedToConnect(error: NetworkConnectionError)
    {
        Debug.Log("Could not connect to server: "+ error);
    }

    function OnPlayerConnected(player: NetworkPlayer) {
        Debug.Log("Player connected from: " + player.ipAddress +":" + player.port);
    }


    function OnPlayerDisconnected(player: NetworkPlayer) {
        Debug.Log("Player disconnected from: " + player.ipAddress+":" + player.port);
    }

    function OnFailedToConnectToMasterServer(info: NetworkConnectionError)
    {
        Debug.Log("Could not connect to master server: "+ info);
    }

    function OnNetworkInstantiate (info : NetworkMessageInfo) {
        Debug.Log("New object instantiated by " + info.sender);
    }
    */

    function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo)
    {
        for(var i:int = 0;i<networkClasses.length;i++)
    {
            var networkClass:NetworkBehaviour = networkClasses[i] as NetworkBehaviour;
            networkClass.OnSerializeNetworkView(stream,info);
        }
    }


    /**
     * Register networkClasses & functions with Networking class
     */

    function register(networkClass:MonoBehaviour)
    {
        networkClasses.Push(networkClass);
    }

    function registerRPC(networkClass:String, funcName:String, func:Function)
    {
        try {
            print("Adding: " + funcName);
            rpc_functions.Add(networkClass + "_" + funcName,func);
        } catch(err:System.Exception){
            print(typeof err);
            print(err.Message);
        }
    }


    /**
     * execute rpc functions
     */

    function callRPC(networkClass:String,funcName:String,indusimRPCMode:RPCMode)
    {
        print("Calling: " + funcName);
        networkView.RPC("networkRPC",indusimRPCMode,networkClass+ "_" +funcName);
    }


    function callRPC(networkClass:String,funcName:String,indusimPlayer:NetworkPlayer)
    {
        networkView.RPC("networkRPC",indusimPlayer,networkClass+ "_" +funcName);
    }

    @RPC
    function networkRPC(class_funcName_params:String)
    {
        var splitted:String[] = class_funcName_params.Split(["("],System.StringSplitOptions.None);
        var class_funcName:String;
        if(splitted.length > 1 && splitted[1].length > 1){
            class_funcName = splitted[0];
            var rest:String = splitted[1];
            rest = rest.Substring(0,rest.length-1);
            var params:String[] = rest.Split(","[0]);
            if(rpc_functions.ContainsKey(class_funcName))
            {
                (rpc_functions[class_funcName] as Function)(params);
            }

        } else {
            class_funcName = splitted[0];
            if(rpc_functions.ContainsKey(class_funcName))
            {
                (rpc_functions[class_funcName] as Function)();
            }

        }
    }

    @RPC
    function debugLog( msg : String)
    {
       Debug.Log("RPC: " + msg);
    }

    @RPC
    function playerID( id : int)
    {
        if(userID == -1)
        {
            userID = id;
            Debug.Log("I just got uid: " + userID);
            networkView.RPC("acceptPlayerID",RPCMode.Server,userID);
        }
    }

    @RPC
    function acceptPlayerID( id:int, info:NetworkMessageInfo)
    {
        Debug.Log(info.sender.ToString() + " just got #" + id);
    }

    @RPC
    function getPlayerID(info:NetworkMessageInfo)
    {
        if(networkView.isMine)
        {
            networkView.RPC("playerID",info.sender,++_uid);
        }
    }

}
