#pragma strict
class NetworkBehaviour extends MonoBehaviour
{

    protected var networking:Networking;

    protected var className:String = "";

    function Start(){
        className = (typeof this).ToString();

        networking = GetComponent(Networking);






    }

    function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo){

    }

    function registerStateSync(){
        networking.register(this);
    }

    protected function registerRPC(funcName:String,func:Function){
        networking.registerRPC(className, funcName, func);
    }


    protected function sendRPC(funcName:String,mode:RPCMode){
        networking.callRPC(className,funcName,mode);
    }

    protected function sendRPC(funcName:String,player:NetworkPlayer){
        networking.callRPC(className,funcName,player);
    }
}
