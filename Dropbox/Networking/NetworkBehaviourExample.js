#pragma strict
class NetworkBehaviourExample extends NetworkBehaviour {
    var sendMSG:System.Boolean = false;

    var stateSyncCounter:int = 0;

    function Start () {
        /*############
        # IMPORTANT! #
        ############*/
        super.Start();


        /*############################
        # Register the RPC functions #
        ############################*/
        registerRPC("spamMessage", spamMessage);
        registerRPC("spamTest", spamTest);
        registerStateSync();

    }

    function Update(){
        if(networkView.isMine && Network.peerType != NetworkPeerType.Disconnected && (!sendMSG && stateSyncCounter < 100)){
            sendMSG = true;
            StartCoroutine(waitAndSpam(10));
            print(stateSyncCounter);
        }
    }

    function OnGUI(){
        var tekst = String.Format ("ssc: {0:000}", stateSyncCounter);
        GUI.Label (Rect (400, 175, 100, 30), tekst);
    }

    /*############
    # State Sync #
    ############*/
    function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo){
        //initiate temp var
       var ssc:int;
     if (stream.isWriting) {    //writing
            ssc = stateSyncCounter;
            stream.Serialize(ssc);
        } else {
            stream.Serialize(ssc); //reading
            stateSyncCounter = ssc;
        }
    }



    function waitAndSpam(waitTime:float)
    {
        yield WaitForSeconds(waitTime);
        sendRPC("spamTest",RPCMode.All);
        stateSyncCounter++;
        yield WaitForSeconds(waitTime/2);
        sendRPC("spamMessage(Blaat,Spam,muhahaha)",RPCMode.All);
        stateSyncCounter+=3;
    }
    /**
     * Example with params
     */
    function spamMessage(s:String[]){
        for(var i:int =0;i<s.length;i++){
            print(s[i]);

        }
    }

    /**
     * Example without params
     */
     function spamTest(){
        print("Test");


     }
}
