#Netwerken in Unity (free)#
##InduSim##

--------------------------------------------------

###Onderzoeksvragen###
 - Welke data moet er voor ons spel gesynchroniseerd worden?
 - Welke manieren van multiplayer over network ondersteunt Unity?
 - Welke manier pas het beste bij onze wensen?
 - Welke serialisatie kan het beste gebruikt worden om data te versturen? (xml, json serialized)

###Welke data moet er voor ons spel gesynchroniseerd worden?###
 - Chat berichten                       –   *RPC*   –  **Object met UTF8 TEXT**
 - Spionage rapporten                   –   *RPC*   –  **Object met speler info**
 - Goederenvoorraad in stad
 	- Dynamische dorpsmarkt             –   *RPC*   –  **JSON string met hele markt**
 		- In en verkoop van voorwerpen  –   *RPC*   –  **JSON string met grondstoftype en aantal**
 	- Gezocht / Aangeboden door spelers –   *RPC*   –  **Object met grondstof, aantal en prijs**
 		- Accepteren van vraag/aanbod   –   *RPC*   –  **Object met grondstof en aantal**
 - Sabotage Aanval                      –   *RPC*   –  **Bool**
 - Eindstatistieken van speler          –   *RPC*   –  **xml**
 - Events
 	- Spel begin & spel eind            –   *RPC*   –  **Bool**
 	- Tijdsync                          –   *Sync*	–  *Short (jaartal)*
 	- Stad events                       –   *RPC*   –  **JSON Object** `{story:1,demand:{wood:10},reward:{wood:100}}`
 	- Ping pong                         –   *PING*  –  **ping**


###Welke manieren van multiplayer over network ondersteunt Unity?###	
 - Authoritative Server
	- De server doet alle simulaties. Dus de client stuurd naar de server wat de speler wilt doen en de server bepaalt of het kan.
	- De client ontvangt de game status van de server en kan de game status ook niet zelf aanpassen.
 - Non-Authoritative Server
	- Elke client doet zijn eigen input en game logic
	- De resultaten van bepaalde acties worden naar de server gestuurd (bijvoorbeeld producten verkopen)
	- Als er een actie is geweest van een speler dan synchroniseerd de server de andere spelers.
	