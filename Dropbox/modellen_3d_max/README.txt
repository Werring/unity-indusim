De textures moeten bij unity in de textures map.
De modellen kunnen van uit 3ds max ge-exporteerd worden naar unity als een fbx bestand.
Het is ook mogelijk om het max bestand in unity te zetten maar volgens de modelleer docent is het beter om het te exporteren.
In unity klik op het model en zet de scale factor naar 0.255

Het is al gedaan in het project van guido dus daar kan in gekeken worden.
https://gvanhienen@bitbucket.org/gvanhienen/unity-projectgt-guido



Gebruik geen hair/fur in 3ds max.
Je kan er heel snel heel mooi gras of graan mee maken maar er zijn een aantal problemen:
 - Unity ondersteunt het niet en het wordt daar dus ook niet zichtbaar.
 - Hair/fur omzetten eerst naar editable mesh en dan exporteren naar unity werkt, maar er onstaat dan een heleboel poly's.
	Ik had 20.000 poly's voor een model dat in het spel de groote heeft van 1 grid vlak. Dit model vond ik al minimaal.
 - Er waren ook een paar kleur problemen. In 3ds max zag het er goed uit maar de kleur werd niet meegenomen naar unity.
	Voor de kleur is een texture gemaakt en dat werkt alleen is het er niet mooier van geworden.
Zie de modellen om te begrijpen wat ik bedoel.
 - GrassV1 mooi model.
 - GrassV2 grass is verminderd en het is omgezet naar mesh.
 - GrassV3 er is met de material editor een texture aan toegevoegd.





Let op 1 meter in autodesk is het zelfde als een grid vlak.
Rond de modellen af op meters want anders wordt het vervelend om het in unity goed te krijgen.